/* global fetch */

import fetchival from 'fetchival';
import RNFetchBlob from 'react-native-fetch-blob';
import _ from 'lodash';

import apiConfig from './config';
import * as sessionSelectors from '../session/selectors';

export const exceptionExtractError = (exception) => {
    if (!exception.Errors) return false;
    let error = false;
    const errorKeys = Object.keys(exception.Errors);
    if (errorKeys.length > 0) {
        error = exception.Errors[errorKeys[0]][0].message;
    }
    return error;
};

export const fetchApi = (endPoint, payload = {}, method = 'get', headers = {}, contentType = '') => {
    const token = sessionSelectors.getToken();
    const type = sessionSelectors.getType();

    const promise = new Promise((resolve, reject) => {
        const url = `${apiConfig.url}${token ? `${apiConfig.urlType}` : ''}${endPoint}`,
        mergedHeaders = { ...headers, ...(token ? { Authorization: `${type} ${token}` } : { }) };
        let request;
        console.log(url);
        console.log({ headers: mergedHeaders });
        console.log(JSON.stringify(payload));
        console.log(method);

        if (contentType === 'multipart') {
            request = RNFetchBlob.fetch(method.toUpperCase(), url, mergedHeaders, payload);
        } else if (contentType === 'urlencoded') {
            request = fetch(url, { method: method.toLowerCase(), headers: mergedHeaders, body: payload });
        } else {
            request = fetchival(url, { headers: mergedHeaders })[method.toLowerCase()](payload);
        }

        request
            .then((res) => {
                resolve(res);
            })
            .catch((e) => {
                if (e.response && e.response.json) {
                    e.response.json().then((json) => {
                        if (json) {
                            reject(json);
                        } else {
                            reject(e);
                        }
                    });
                } else {
                    reject(e);
                }
            });
    });

    return promise;
};
