import { fetchApi } from '../api/index';
import apiConfig from '../api/config';

const endPoints = {
    login: `login-${apiConfig.urlType}`,
};

const keys = {
    client_id: 5,
    client_secret: 'CpXEqBkBvdl02uYTex7o3n29AbCg3zwhhv3atINn',
    grant_type: 'password',
};

export const authenticate = (username, password) => fetchApi(endPoints.login, { ...keys, username, password }, 'post', {
    // 'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
});
