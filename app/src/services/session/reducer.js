import * as actionTypes from './actionTypes';

const initialState = {
    tokens: {
        access: null,
        type: null,
        refresh: null,
        expiresIn: null,
    },
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE:
            return {
                ...state,
                tokens: { ...action.session },
            };
        default:
            return state;
    }
};
