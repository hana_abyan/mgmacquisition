import { store } from '../../store';

import * as api from './api';
import * as actionCreators from './actions';

// const { store } = configureStore();

const onRequestSuccess = (response) => {
    // const tokens = response.tokens.reduce((prev, item) => ({
    //     ...prev,
    //     [item.type]: item,
    // }), {});
    // store.dispatch(actionCreators.update({ tokens, user: response.user }));
    // setSessionTimeout(tokens.access.expiresIn);

    store.dispatch(actionCreators.update({
        access: response.access_token,
        type: response.token_type,
        refresh: response.refresh_token,
        expiresIn: response.expires_in,
    }));
};

const onRequestFailed = (exception) => {
    // clearSession();
    // throw exception;
    throw exception;
};

export const authenticate = (username, password) =>
    api.authenticate(username, password)
        .then(onRequestSuccess)
        .catch(onRequestFailed);
