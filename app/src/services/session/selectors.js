import { store } from '../../store';

export const getToken = () => store.getState().services.session.tokens.access;

export const getType = () => store.getState().services.session.tokens.type;
