import CheckBox from 'react-native-check-box';

export default class CustomCheckBox extends CheckBox {
    componentWillReceiveProps(nextProps) {
        if (this.props.isChecked !== nextProps.isChecked) {
            this.setState({ isChecked: nextProps.isChecked });
        }
    };

    onClick() {
        this.props.onClick();
    }
}
