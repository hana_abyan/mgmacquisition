import * as actionTypes from './actionTypes';

const initialState = {
    categories: [],
    threads: [],
    detail: {},
    // replies: [],
    selectedCategory: '',
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_CATEGORY:
            return {
                ...state,
                categories: action.data,
            };
        case actionTypes.UPDATE_THREAD:
        case actionTypes.EMPTY_THREAD:
            return {
                ...state,
                threads: action.data,
            };
        case actionTypes.EMPTY_THREAD_DETAIL:
        case actionTypes.UPDATE_THREAD_DETAIL:
            return {
                ...state,
                detail: action.data,
            };
        // case actionTypes.UPDATE_CATEGORY_WITH_REPLIES:
        //     return {
        //         ...state,
        //         replies: action.data,
        //         selectedCategory: action.categoryId,
        //     };
        default:
            return state;
    }
};
