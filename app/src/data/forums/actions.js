import * as api from './api';
import * as actionTypes from './actionTypes';

const updateCategory = data => ({
    type: actionTypes.UPDATE_CATEGORY,
    data,
});

const updateThread = data => ({
    type: actionTypes.UPDATE_THREAD,
    data,
});

export const emptyThread = () => ({
    type: actionTypes.EMPTY_THREAD,
    data: [],
});

const updateDetail = data => ({
    type: actionTypes.UPDATE_THREAD_DETAIL,
    data,
});

export const emptyDetail = () => ({
    type: actionTypes.EMPTY_THREAD_DETAIL,
    data: [],
});

// const updateCategoryWithReplies = (data, categoryId) => ({
//     type: actionTypes.UPDATE_CATEGORY_WITH_REPLIES,
//     data,
//     categoryId,
// });

export const getCategory = payload =>
    dispatch =>
        api.getCategory(payload).then((response) => {
            dispatch(updateCategory(response));
            return response;
        }).catch((e) => { throw e; });

export const getThread = payload =>
    dispatch =>
        api.getThread(payload).then((response) => {
            dispatch(updateThread(response));
            return response;
        }).catch((e) => { throw e; });

export const getDetail = payload =>
    dispatch =>
        api.getDetail(payload).then((response) => {
            dispatch(updateDetail(response));
            return response;
        }).catch((e) => { throw e; });

// export const getCategoryWithReplies = ({ categoryId }) =>
//     dispatch =>
//         api.getCategoryWithReplies().then((response) => {
//             dispatch(updateCategoryWithReplies(response.data, categoryId));
//             return response;
//         }).catch((e) => { throw e; });
