import * as api from './api';

export const postReply = payload =>
    api.postReply(payload).then(response => response).catch((e) => { throw e; });

export const postThread = payload =>
    api.postThread(payload).then(response => response).catch((e) => { throw e; });

export const postSearch = payload =>
    api.postSearch(payload).then(response => response).catch((e) => { throw e; });
