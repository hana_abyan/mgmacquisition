import { fetchApi } from '../../services/api/index';

const endPoints = {
    getCategory: '/forum-categories',
    getThread: '/forum-categories/:categoryId/threads',
    getDetail: '/forum-thread-replies-images/:threadId',
    // getCategoryWithReplies: '/forum-categories-threads-replies',
    postReply: '/new-reply/:threadId',
    postSearch: '/search/forum',
    postThread: '/new-thread',
};

export const getCategory = () => fetchApi(endPoints.getCategory);

export const getThread = payload => fetchApi(endPoints.getThread.replace(':categoryId', payload.categoryId));

export const getDetail = payload => fetchApi(endPoints.getDetail.replace(':threadId', payload.threadId));

// export const getCategoryWithReplies = () => fetchApi(endPoints.getCategoryWithReplies);

export const postReply = payload => fetchApi(endPoints.postReply.replace(':threadId', payload.find(item => item.name === 'thread_id').data), payload, 'post', {}, 'multipart');

export const postSearch = payload => fetchApi(endPoints.postSearch, payload, 'post');

export const postThread = payload => fetchApi(endPoints.postThread, payload, 'post', {}, 'multipart');
