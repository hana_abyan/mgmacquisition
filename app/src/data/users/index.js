import * as api from './api';

export const changePassword = payload =>
    api.changePassword(payload).then(response => response).catch((e) => { throw e; });
