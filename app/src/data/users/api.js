import { fetchApi } from '../../services/api/index';

const endPoints = {
    changePassword: '/change-password',
};

export const changePassword = payload => fetchApi(endPoints.changePassword, payload, 'post');
