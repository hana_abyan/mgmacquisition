import * as api from './api';
import * as actionTypes from './actionTypes';

const updateMenu = list => ({
    type: actionTypes.UPDATE_MENU,
    list,
});

const updateTask = list => ({
    type: actionTypes.UPDATE_TASK,
    list,
});

const updateChart = (list, petakId) => ({
    type: actionTypes.UPDATE_CHART,
    list,
    petakId,
});

const updateMangroveNote = list => ({
    type: actionTypes.UPDATE_MANGROVE_NOTE,
    list,
});

const updateImage = list => ({
    type: actionTypes.UPDATE_IMAGE,
    list,
});

const updateHomeKey = key => ({
    type: actionTypes.UPDATE_HOME_KEY,
    key,
});

export const setUploadImageStatus = (isPosting, uploadStatus, payload) => ({
    type: actionTypes.UPLOAD_IMAGE_STATUS,
    isPosting,
    uploadStatus,
    payload,
});

export const getMenu = () =>
    dispatch =>
        api.getMenu().then(response => dispatch(updateMenu(response))).catch((e) => { throw e; });

export const getTask = payload =>
    dispatch =>
        api.getTask(payload).then((response) => {
            dispatch(updateTask(response));
            return response;
        }).catch((e) => { throw e; });

export const getChart = payload =>
    dispatch =>
        api.getChart(payload).then((response) => {
            dispatch(updateChart(response, payload.petak_id));
            return response;
        }).catch((e) => { throw e; });

export const getNote = payload =>
    dispatch =>
        api.getNote(payload).then((response) => {
            dispatch(updateMangroveNote(response.data));
            return response;
        }).catch((e) => { throw e; });

export const getImage = payload =>
    dispatch =>
        api.getImage(payload).then((response) => {
            dispatch(updateImage(response));
            return response;
        }).catch((e) => { throw e; });

export const setHomeKey = key =>
    dispatch =>
        dispatch(updateHomeKey(key));
