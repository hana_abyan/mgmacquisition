import { store } from '../../store';

export const getSelectedCluster = () => store.getState().clusters.selectedCluster;

export const getMenu = () => store.getState().clusters.menu;
