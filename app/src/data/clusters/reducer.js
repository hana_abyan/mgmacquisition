import * as actionTypes from './actionTypes';
// import { data } from '../../data/dummy/menu'; // temporary

const initialState = {
    menu: [],
    task: {},
    chart: {},
    note: [],
    homeKey: null,
    image: [],
    isPosting: false,
    uploadStatus: false,
    uploadResult: {},
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_MENU:
            return {
                ...state,
                menu: action.list,
                // menu: data, // temporary
            };
        case actionTypes.UPDATE_TASK:
            return {
                ...state,
                task: action.list,
            };
        case actionTypes.UPDATE_CHART:
            return {
                ...state,
                chart: action.list,
            };
        case actionTypes.UPDATE_MANGROVE_NOTE:
            return {
                ...state,
                note: action.list,
            };
        case actionTypes.UPDATE_IMAGE:
            return {
                ...state,
                image: action.list,
            };
        case actionTypes.UPDATE_HOME_KEY:
            return {
                ...state,
                homeKey: action.key,
            };
        case actionTypes.UPLOAD_IMAGE_STATUS:
            return {
                ...state,
                isPosting: action.isPosting,
                uploadStatus: action.uploadStatus,
                uploadResult: action.payload,
            };
        default:
            return state;
    }
};
