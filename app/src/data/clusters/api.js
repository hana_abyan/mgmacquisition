import { fetchApi } from '../../services/api/index';

const endPoints = {
    get: '/area-cluster-petak',
    getMenu: '/sidebar-cluster-petak',
    getTask: '/tasks/:petakId',
    editTask: '/task-detail',
    getChart: '/getChart',
    getChartByPeriod: '/chart-details/:chartId',
    postChart: '/chart',
    getNote: '/panen/:petakId/keterangan',
    addNote: '/update-keterangan/:noteId',
    getImage: '/panen-images/:petakId',
    postImage: '/panen-images',
    deleteImage: '/panen-images',
};

export const get = payload => fetchApi(endPoints.get, payload, 'get');

export const getMenu = () => fetchApi(endPoints.getMenu);

export const getTask = payload => fetchApi(endPoints.getTask.replace(':petakId', payload.petakId));

export const editTask = payload => fetchApi(endPoints.editTask, payload, 'post');

export const getChart = payload => fetchApi(endPoints.getChart, payload, 'post');

export const getChartByPeriod = payload => fetchApi(endPoints.getChartByPeriod.replace(':chartId', payload.chartId), payload, 'post');

export const postChart = payload => fetchApi(endPoints.postChart, payload, 'post', { 'Content-Type': 'application/json' });

export const getNote = payload => fetchApi(endPoints.getNote.replace(':petakId', payload.petakId));

export const postNote = payload => fetchApi(endPoints.addNote.replace(':noteId', payload.noteId), payload, 'post');

export const getImage = payload => fetchApi(endPoints.getImage.replace(':petakId', payload.petakId));

export const postImage = payload => fetchApi(endPoints.postImage, payload, 'post', {}, 'multipart');

export const deleteImage = payload => fetchApi(endPoints.deleteImage, payload, 'delete', { 'Content-Type': 'application/x-www-form-urlencoded' }, 'urlencoded');
