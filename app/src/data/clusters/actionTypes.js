export const UPDATE_PROGRESS = 'clusters/UPDATE_PROGRESS';
export const UPDATE_MENU = 'clusters/UPDATE_MENU';
export const UPDATE_TASK = 'clusters/UPDATE_TASK';
export const UPDATE_CHART = 'clusters/UPDATE_CHART';
export const UPDATE_MANGROVE_NOTE = 'clusters/UPDATE_MANGROVE_NOTE';
export const UPDATE_HOME_KEY = 'clusters/UPDATE_HOME_KEY';
export const UPDATE_IMAGE = 'clusters/UPDATE_IMAGE';
export const UPLOAD_IMAGE_STATUS = 'clusters/UPLOAD_IMAGE_STATUS';
