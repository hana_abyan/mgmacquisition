import * as api from './api';

export const editTask = payload =>
    api.editTask(payload).then(response => response).catch((e) => { throw e; });

export const createChart = payload =>
    api.postChart(payload).then(response => response).catch((e) => { throw e; });

export const updateNote = payload =>
    api.postNote(payload).then(response => response).catch((e) => { throw e; });

export const getChartByPeriod = payload =>
    api.getChartByPeriod(payload).then(response => response).catch((e) => { throw e; });

export const postImage = payload =>
    api.postImage(payload).then(response => response).catch((e) => { throw e; });

export const deleteImage = payload =>
    api.deleteImage(payload).then(response => response).catch((e) => { throw e; });
