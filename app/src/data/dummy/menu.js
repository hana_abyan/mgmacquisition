export const data = [
    {
        cluster_id: 2,
        name: 'Cluster 2',
        image: 'http://mandiri.shideichis.me/storage/aejDPG0Xp2aduYFtM5IGtRHjq6gvDt0ydq2HXvEX.png',
        petak: [
            {
                petak_id: 22,
                name: 'Petak 8A',
                progress: 40,
                type: 'Swath',
                luas: 0,
                due_date: '2018-02-09 14:19:38',
                due_date_view: '09 February 2018',
                image: 'http://mandiri.shideichis.me/storage/tkRk7yFNI2agRr0oY8pmsfBgSbeGJpVqAiohDZRL.png',
            },
            {
                petak_id: 23,
                name: 'Mangrove 8B',
                progress: 40,
                type: 'Swath',
                luas: 0,
                due_date: '2018-02-09 14:19:38',
                due_date_view: '09 February 2018',
                image: 'http://mandiri.shideichis.me/storage/tkRk7yFNI2agRr0oY8pmsfBgSbeGJpVqAiohDZRL.png',
            },
            {
                petak_id: 24,
                name: 'Mangrove 8',
                progress: 40,
                type: 'Mangrove',
                luas: 0,
                due_date: '2018-02-09 14:19:38',
                due_date_view: '09 February 2018',
                image: 'http://mandiri.shideichis.me/storage/tkRk7yFNI2agRr0oY8pmsfBgSbeGJpVqAiohDZRL.png',
            },
        ],
    },
];
