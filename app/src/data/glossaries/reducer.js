import * as actionTypes from './actionTypes';

const initialState = {
    list: [],
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE:
            return {
                ...state,
                list: action.list,
            };
        default:
            return state;
    }
};
