import * as actionTypes from './actionTypes';
import * as api from './api';

const update = list => ({
    type: actionTypes.UPDATE,
    list,
});

export const get = payload =>
    dispatch =>
        api.get(payload).then(response => dispatch(update(response.data))).catch((e) => { throw e; });
