import { fetchApi } from '../../services/api/index';

const endPoints = {
    get: '/dictionaries',
};

export const get = payload => fetchApi(endPoints.get, payload, 'get');
