import * as actionTypes from './actionTypes';

const initialState = {
    list: [],
    detail: [],
    category: [],
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE:
            return {
                ...state,
                list: action.data,
            };
        case actionTypes.UPDATE_DETAIL:
            return {
                ...state,
                detail: action.data,
            };
        case actionTypes.UPDATE_CATEGORY:
            return {
                ...state,
                category: action.data,
            };
        default:
            return state;
    }
};
