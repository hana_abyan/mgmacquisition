import { fetchApi } from '../../services/api/index';

const endPoints = {
    get: '/articles',
    // detail: '/articles/',
    getCategory: '/categories',
    getByCategory: '/articles-by-category/:categoryId',
    search: '/search/artikel', // post
};

export const getAll = payload => fetchApi(endPoints.get, payload, 'get');

// export const getDetail = payload => fetchApi(`${endPoints.detail}${payload.id}`, {}, 'get');

export const getCategory = payload => fetchApi(endPoints.getCategory, payload);

export const getByCategory = payload => fetchApi(endPoints.getByCategory.replace(':categoryId', payload.categoryId));

export const postSearch = payload => fetchApi(endPoints.search, payload, 'post');
