import * as api from './api';
import * as actionTypes from './actionTypes';

const update = data => ({
    type: actionTypes.UPDATE,
    data,
});

// const updateDetail = data => ({
//     type: actionTypes.UPDATE_DETAIL,
//     data,
// });

const updateCategory = data => ({
    type: actionTypes.UPDATE_CATEGORY,
    data,
});

// const updateByCategory = data => ({
//     type: actionTypes.UPDATE_BY_CATEGORY,
//     data,
// });

// const updateSearch = data => ({
//     type: actionTypes.UPDATE_SEARCH,
//     data,
// });

export const getAll = payload =>
    dispatch =>
        api.getAll(payload).then(response => dispatch(update(response))).catch((e) => { throw e; });

// export const getDetail = payload =>
//     dispatch =>
//         api.getDetail(payload).then(response => dispatch(updateDetail(response.data))).catch((e) => { throw e; });

export const getCategory = payload =>
    dispatch =>
        api.getCategory(payload).then(response => dispatch(updateCategory(response.data))).catch((e) => { throw e; });

export const getByCategory = payload =>
    dispatch =>
        api.getByCategory(payload).then(response => dispatch(update(response))).catch((e) => { throw e; });

export const postSearch = payload =>
    dispatch =>
        api.postSearch(payload).then(response => dispatch(update(response))).catch((e) => { throw e; });
