import { combineReducers } from 'redux';
import { reducer as glossariesReducer } from './glossaries/reducer';

export const reducer = combineReducers({
    glossaries: glossariesReducer,
});
