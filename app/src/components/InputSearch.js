import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#E7E7E7',
    },
    iconContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    icon: {
        paddingLeft: 10,
    },
    input: {
        flex: 1,
        paddingLeft: 10,
        backgroundColor: 'white',
        fontSize: 20,
        color: '#777',
        fontFamily: 'MyriadProRegular',
    },
    indicatorHolder: {
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'center',
        paddingRight: 5,
    },
});

const InputSearch = ({
    placeholder, changeEvent, value, onSubmit, isLoading,
}) => (
    <View style={styles.container}>
        <View
            style={styles.iconContainer}
        >
            <Icon
                name="search"
                size={30}
                color="#A1A1A1"
                style={styles.icon}
            />
        </View>
        <TextInput
            style={styles.input}
            placeholder={placeholder}
            onChangeText={text => changeEvent(text)}
            value={value}
            underlineColorAndroid="transparent"
            onSubmitEditing={() => onSubmit()}
            returnKeyType="search"
        />
        <View
            style={styles.indicatorHolder}
        >
            {
                isLoading &&
                <ActivityIndicator
                    size="large"
                    color="#1A537C"
                />
            }
        </View>
    </View>
);

InputSearch.propTypes = {
    placeholder: PropTypes.string,
    changeEvent: PropTypes.func.isRequired,
    value: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
};

InputSearch.defaultProps = {
    placeholder: '',
    value: '',
    isLoading: false,
};

export default InputSearch;
