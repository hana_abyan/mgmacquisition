import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
    },
    text: {
        fontSize: 16,
    },
    textRed: {
        color: 'red',
    },
    textGreen: {
        color: 'green',
    },
});

const Message = ({ type, message }) => (
    <View style={styles.container}>
        <Text
            style={[styles.text, (type === 'error' ? styles.textRed : styles.textGreen)]}
        >{message}
        </Text>
    </View>
);

Message.propTypes = {
    type: PropTypes.string,
    message: PropTypes.string,
};

Message.defaultProps = {
    type: 'error',
    message: '',
};

export default Message;
