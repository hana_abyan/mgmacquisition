import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    StyleSheet,
    Text,
    StatusBar,
    View
} from 'react-native';
import { ThemeProvider, Toolbar, Drawer, Avatar } from 'react-native-material-ui';
import DrawerSection from '../../libraries/react-native-material-ui/DrawerSection';
import Container from '../Container';
import { uiTheme } from '../../core/config';

import * as clustersActionCreators from '../../data/clusters/actions';

@connect(
    state => ({
        clusters: state.clusters.list,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
export default class DrawerMenu extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            active: 'people',
        };
    }

    _setInfoActive() {
        this.setState({ active: 'info' });
    }

    render() {
        const { clusters } = this.props;
        return (
            <ThemeProvider uiTheme={uiTheme}>
                <Container>
                    <StatusBar backgroundColor="rgba(0, 0, 0, 0.2)" translucent />
                    <Toolbar
                        leftElement="arrow-back"
                        onLeftElementPress={() => this.props.navigation.navigate('DrawerClose')}
                        centerElement="Menu"
                    />
                    <View style={styles.container}>
                        <Drawer>
                            {/* <Drawer.Header >
                                <Drawer.Header.Account
                                    avatar={<Avatar text="A" />}
                                    accounts={[
                                        { avatar: <Avatar text="B" /> },
                                        { avatar: <Avatar text="C" /> },
                                    ]}
                                    // footer={{
                                    //     dense: true,
                                    //     centerElement: {
                                    //         primaryText: 'Reservio',
                                    //         secondaryText: 'business@email.com',
                                    //     },
                                    //     rightElement: 'arrow-drop-down',
                                    // }}
                                />
                                <Text>Test</Text>
                            </Drawer.Header> */}

                            <DrawerSection
                                style={{
                                    label: { color: '#0000ff', margin: 0, padding: 0 },
                                    icon: { margin: 0, padding: 0 },
                                    item: { margin: 0, padding: 0 },
                                }}
                                items={[
                                    {
                                        key: 'home',
                                        icon: 'home',
                                        value: 'Home',
                                        active: this.state.active === 'home',
                                        onPress: () => {
                                            this.setState({ active: 'home' });
                                            this.props.navigation.navigate('Home');
                                        },
                                    },
                                ]}
                            />
                                <DrawerSection
                                    style={{
                                        label: { color: '#0000ff' },
                                        container: { paddingLeft: 70 },
                                    }}
                                    items={[
                                        {
                                            key: 'cluster1',
                                            value: clusters.length > 0 ? clusters[0].petak.name : 'Petak 1',
                                            active: this.state.active === 'petak1',
                                            onPress: () => {
                                                this.setState({ active: 'petak1' });
                                                this.props.actions.setActiveCluster({ id: clusters[0].id, name: clusters[0].petak.name });
                                                this.props.navigation.navigate('ClusterMenu');
                                            },
                                        },
                                    ]}
                                />

                                    <DrawerSection
                                        style={{
                                            label: { color: '#0000ff' },
                                            container: { paddingLeft: 100 },
                                        }}
                                        items={[
                                            {
                                                key: 'cluster1a',
                                                value: 'Progress List',
                                                active: this.state.active === 'petak1a',
                                                onPress: () => {
                                                    this.setState({ active: 'petak1a' });
                                                    this.props.actions.setActiveCluster({ id: clusters[0].id, name: clusters[0].petak.name });
                                                    this.props.navigation.navigate('UpdateProgress');
                                                },
                                            },
                                            {
                                                key: 'cluster1b',
                                                value: 'Input Kualitas Air',
                                                active: this.state.active === 'petak1b',
                                                onPress: () => {
                                                    this.setState({ active: 'petak1b' });
                                                    this.props.actions.setActiveCluster({ id: clusters[0].id, name: clusters[0].petak.name });
                                                    this.props.navigation.navigate('InputWaterQuality');
                                                },
                                            },
                                            {
                                                key: 'cluster1c',
                                                value: 'Input Data Udang',
                                                active: this.state.active === 'petak1c',
                                                onPress: () => {
                                                    this.setState({ active: 'petak1c' });
                                                    this.props.actions.setActiveCluster({ id: clusters[0].id, name: clusters[0].petak.name });
                                                    this.props.navigation.navigate('InputShrimpData');
                                                },
                                            },
                                            {
                                                key: 'cluster1d',
                                                value: 'Foto Petak',
                                                active: this.state.active === 'petak1d',
                                                onPress: () => {
                                                    this.setState({ active: 'petak1d' });
                                                    this.props.actions.setActiveCluster({ id: clusters[0].id, name: clusters[0].petak.name });
                                                    this.props.navigation.navigate('ClusterPictures');
                                                },
                                            },
                                        ]}
                                    />

                                <DrawerSection
                                    style={{
                                        label: { color: '#0000ff' },
                                        container: { paddingLeft: 70 },
                                    }}
                                    items={[
                                        {
                                            key: 'cluster2',
                                            value: clusters.length > 0 ? clusters[1].petak.name : 'Petak 2',
                                            active: this.state.active === 'petak2',
                                            onPress: () => {
                                                this.setState({ active: 'petak2' });
                                                this.props.actions.setActiveCluster({ id: clusters[1].id, name: clusters[1].petak.name });
                                                this.props.navigation.navigate('ClusterMenu');
                                            },
                                        },
                                    ]}
                                />

                                    <DrawerSection
                                        style={{
                                            label: { color: '#0000ff' },
                                            container: { paddingLeft: 100 },
                                        }}
                                        items={[
                                            {
                                                key: 'cluster2a',
                                                value: 'Progress List',
                                                active: this.state.active === 'petak2a',
                                                onPress: () => {
                                                    this.setState({ active: 'petak2a' });
                                                    this.props.actions.setActiveCluster({ id: clusters[1].id, name: clusters[1].petak.name });
                                                    this.props.navigation.navigate('UpdateProgress');
                                                },
                                            },
                                            {
                                                key: 'cluster2b',
                                                value: 'Input Kualitas Air',
                                                active: this.state.active === 'petak2b',
                                                onPress: () => {
                                                    this.setState({ active: 'petak2b' });
                                                    this.props.actions.setActiveCluster({ id: clusters[1].id, name: clusters[1].petak.name });
                                                    this.props.navigation.navigate('InputWaterQuality');
                                                },
                                            },
                                            {
                                                key: 'cluster2c',
                                                value: 'Input Data Udang',
                                                active: this.state.active === 'petak2c',
                                                onPress: () => {
                                                    this.setState({ active: 'petak2c' });
                                                    this.props.actions.setActiveCluster({ id: clusters[1].id, name: clusters[1].petak.name });
                                                    this.props.navigation.navigate('InputShrimpData');
                                                },
                                            },
                                            {
                                                key: 'cluster2d',
                                                value: 'Foto Petak',
                                                active: this.state.active === 'petak2d',
                                                onPress: () => {
                                                    this.setState({ active: 'petak2d' });
                                                    this.props.actions.setActiveCluster({ id: clusters[1].id, name: clusters[1].petak.name });
                                                    this.props.navigation.navigate('ClusterPictures');
                                                },
                                            },
                                        ]}
                                    />

                                <DrawerSection
                                    style={{
                                        label: { color: '#0000ff' },
                                        container: { paddingLeft: 70 },
                                    }}
                                    items={[
                                        {
                                            key: 'cluster3',
                                            value: clusters.length > 0 ? clusters[2].petak.name : 'Mangrove',
                                            active: this.state.active === 'mangrove',
                                            onPress: () => {
                                                this.setState({ active: 'mangrove' });
                                                this.props.actions.setActiveCluster({ id: clusters[2].id, name: clusters[2].petak.name });
                                                this.props.navigation.navigate('ClusterMenu');
                                            },
                                        },
                                    ]}
                                />

                                    <DrawerSection
                                        style={{
                                            label: { color: '#0000ff' },
                                            container: { paddingLeft: 100 },
                                        }}
                                        items={[
                                            {
                                                key: 'cluster3a',
                                                value: 'Progress List',
                                                active: this.state.active === 'mangrovea',
                                                onPress: () => {
                                                    this.setState({ active: 'mangrovea' });
                                                    this.props.actions.setActiveCluster({ id: clusters[2].id, name: clusters[2].petak.name });
                                                    this.props.navigation.navigate('UpdateProgress');
                                                },
                                            },
                                            {
                                                key: 'cluster3b',
                                                value: 'Input Kualitas Air',
                                                active: this.state.active === 'mangroveb',
                                                onPress: () => {
                                                    this.setState({ active: 'mangroveb' });
                                                    this.props.actions.setActiveCluster({ id: clusters[2].id, name: clusters[2].petak.name });
                                                    this.props.navigation.navigate('InputWaterQuality');
                                                },
                                            },
                                            {
                                                key: 'cluster3c',
                                                value: 'Input Data Udang',
                                                active: this.state.active === 'mangrovec',
                                                onPress: () => {
                                                    this.setState({ active: 'mangrovec' });
                                                    this.props.actions.setActiveCluster({ id: clusters[2].id, name: clusters[2].petak.name });
                                                    this.props.navigation.navigate('InputShrimpData');
                                                },
                                            },
                                            {
                                                key: 'cluster3d',
                                                value: 'Foto Petak',
                                                active: this.state.active === 'mangroved',
                                                onPress: () => {
                                                    this.setState({ active: 'mangroved' });
                                                    this.props.actions.setActiveCluster({ id: clusters[2].id, name: clusters[2].petak.name });
                                                    this.props.navigation.navigate('ClusterPictures');
                                                },
                                            },
                                        ]}
                                    />

                            <DrawerSection
                                items={[
                                    {
                                        key: 'info',
                                        icon: 'info-outline',
                                        value: 'Info',
                                        active: this.state.active === 'info',
                                        onPress: () => {
                                            this.setState({ active: 'info' });
                                            this.props.navigation.navigate('Info');
                                        },
                                    },
                                    {
                                        key: 'forum',
                                        icon: 'forum',
                                        value: 'Forum',
                                        active: this.state.active === 'forum',
                                        onPress: () => {
                                            this.setState({ active: 'forum' });
                                            this.props.navigation.navigate('Forum');
                                        },
                                    },
                                    {
                                        key: 'glossary',
                                        icon: 'import-contacts',
                                        value: 'Istilah',
                                        active: this.state.active === 'istilah',
                                        onPress: () => {
                                            this.setState({ active: 'istilah' });
                                            this.props.navigation.navigate('Istilah');
                                        },
                                    },
                                ]}
                            />
                        </Drawer>
                    </View>
                </Container>
            </ThemeProvider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    header: {
        backgroundColor: '#455A64',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
