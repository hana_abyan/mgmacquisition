import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const HeaderMenu = ({ navigation }) => (
    <TouchableOpacity
        onPress={() => navigation.navigate('Home')}
    >
        <Icon
            name="md-arrow-back"
            size={30}
            color="#0F7AC9"
            style={{ marginLeft: 20 }}
        />
    </TouchableOpacity>
);

HeaderMenu.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    }).isRequired,
};

export default HeaderMenu;
