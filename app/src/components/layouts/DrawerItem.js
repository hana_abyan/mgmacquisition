import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, TouchableNativeFeedback, Image } from 'react-native';

const styles = StyleSheet.create({
    inner: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 13,
        paddingTop: 11,
        paddingBottom: 11,
    },
    title: {
        fontSize: 20,
        fontWeight: '500',
        color: '#1A537C',
        paddingLeft: 14,
        fontFamily: 'MyriadProBold',
    },
    icon: {
        width: 27,
        height: 27,
    },
});

const DrawerItem = ({
    title, icon, indent, textType, pressEvent,
}) => {
    const customTextStyle = {};

    if (textType === 'medium') {
        Object.assign(customTextStyle, { fontWeight: 'normal', fontFamily: 'MyriadProRegular', fontSize: 19 });
    } else if (textType === 'small') {
        Object.assign(customTextStyle, { fontWeight: 'normal', fontFamily: 'MyriadProRegular', fontSize: 17 });
    }

    return (
        <TouchableNativeFeedback
            background={TouchableNativeFeedback.SelectableBackground()}
            onPress={() => pressEvent()}
        >
            <View
                style={[styles.inner, { paddingLeft: indent }]}
            >
                <Image
                    source={icon}
                    style={styles.icon}
                />
                <Text
                    style={[styles.title, customTextStyle ]}
                >{title}
                </Text>
            </View>
        </TouchableNativeFeedback>
    )
};


DrawerItem.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.number,
    indent: PropTypes.number,
    textType: PropTypes.string,
    pressEvent: PropTypes.func,
};

DrawerItem.defaultProps = {
    icon: null,
    indent: 13,
    textType: 'default',
    pressEvent: () => {},
};

export default DrawerItem;
