import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    StyleSheet,
    Text,
    StatusBar,
    View,
    Image,
} from 'react-native';
import { ThemeProvider, Toolbar, Drawer } from 'react-native-material-ui';
import Container from '../Container';
import DrawerItem from './DrawerItem';
import { uiTheme } from '../../core/config';
import * as clustersActionCreators from '../../data/clusters/actions';

const Logo = require('../../assets/images/logo_mgm.png');
const HomeIcon = require('../../assets/images/icon_beranda.png');
const InfoIcon = require('../../assets/images/icon_info.png');
const ForumIcon = require('../../assets/images/icon_forum.png');
const IstilahIcon = require('../../assets/images/icon_istilah.png');


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    header: {
        backgroundColor: '#455A64',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    profileContainer: {
        borderBottomWidth: 1,
        borderColor: '#e8e8e6',
        paddingBottom: 30,
        marginTop: 20,
        marginBottom: 15,
    },
    profile: {
        flexDirection: 'row',
        paddingLeft: 13,
        alignItems: 'center',
    },
    profileImage: {
        width: 75,
        height: 75,
    },
    profileInfo: {
        marginLeft: 15,
    },
    titleTop: {
        color: '#1A537C',
        fontSize: 22,
        fontFamily: 'MyriadProRegular',
    },
    titleBottom: {
        color: '#1A537C',
        fontSize: 22,
        fontFamily: 'MyriadProBold',
    },
});

const listMenu = [
    {
        id: 0,
        title: 'Beranda',
        icon: HomeIcon,
        routeTo: 'Home',
        group: [],
    },
    {
        id: 1,
        title: 'Info',
        icon: InfoIcon,
        routeTo: 'Info',
    },
    {
        id: 2,
        title: 'Forum',
        icon: ForumIcon,
        routeTo: 'Forum',
    },
    {
        id: 3,
        title: 'Istilah',
        icon: IstilahIcon,
        routeTo: 'Istilah',
    },
    {
        id: 4,
        title: 'Edit Password',
        // icon: IstilahIcon,
        routeTo: 'ChangePassword',
    },
];

@connect(
    state => ({
        menu: state.clusters.menu,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
export default class DrawerMenu extends Component {
    static _titleFormater(text) {
        const arrText = text.split(' ');

        if (arrText.length > 1) {
            return `${arrText[0]} ${arrText[1].toUpperCase()}`;
        }

        return text;
    }

    static _buildClusterMenu(data) {
        if (data.length > 0) {
            return data.reduce((prev, curr) => {
                let children = [];

                if ('petak' in curr && curr.petak.length > 0) {
                    children = curr.petak.map(petak => ({
                        id: `petak-${petak.petak_id}`,
                        title: this._titleFormater(petak.name),
                        routeTo: petak.type.toLowerCase() === 'swath' ? 'ClusterMenu' : 'MangroveMenu',
                        children: petak.type.toLowerCase() === 'swath'
                            ? [
                                { id: `pr-${petak.petak_id}`, title: 'Progress List', routeTo: 'UpdateProgress', params: { petakId: petak.petak_id } },
                                { id: `wq-${petak.petak_id}`, title: 'Input Kualitas Air', routeTo: 'WaterQualityReport', params: { petakId: petak.petak_id } },
                                { id: `sh-${petak.petak_id}`, title: 'Input Data Udang', routeTo: 'ShrimpReport', params: { petakId: petak.petak_id } },
                                { id: `gl-${petak.petak_id}`, title: 'Foto Petak', routeTo: 'PictureGallery', params: { petakId: petak.petak_id, name: this._titleFormater(petak.name) } },
                            ]
                            : [
                                { id: `pr-${petak.petak_id}`, title: 'Progress List', routeTo: 'UpdateProgress', params: { petakId: petak.petak_id } },
                                { id: `mg-${petak.petak_id}`, title: 'Input Data Ketinggian', routeTo: 'MangroveGrowth', params: { petakId: petak.petak_id } },
                                { id: `mn-${petak.petak_id}`, title: 'Input Data Keterangan', routeTo: 'MangroveNote', params: { petakId: petak.petak_id } },
                                { id: `gl-${petak.petak_id}`, title: 'Foto Petak', routeTo: 'PictureGallery', params: { petakId: petak.petak_id, name: this._titleFormater(petak.name) } },
                            ],
                        params: petak,
                    }));
                }

                return [...prev, {
                    id: `cluster-${curr.cluster_id}`,
                    title: this._titleFormater(curr.name),
                    children,
                }];
            }, []);
        }

        return data;
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            menu: listMenu,
        };
    }

    componentWillMount() {
        if (this.props.menu.length > 0) {
            this._updateMenu(this.props.menu);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.menu !== nextProps.menu) {
            this._updateMenu(nextProps.menu);
        }
    }

    _updateMenu(data) {
        const menu = JSON.parse(JSON.stringify(this.state.menu)),
        clusterMenu = this.constructor._buildClusterMenu(data);

        menu[0].group = clusterMenu;

        this.setState({ menu });
        // if (data.length > 0) {
        //     const menu = [...this.state.menu],
        //         [{ name, petak: [petak0, petak1, petak2] }] = data;

        //     /** cluster name */
        //     menu[0].group[0].title = this.constructor._titleFormater(name);

        //     /** petak 1 */
        //     menu[0].group[0].children[0].title = this.constructor._titleFormater(petak0.name);
        //     menu[0].group[0].children[0].params = petak0;

        //     menu[0].group[0].children[0].children[0].params = { petakId: petak0.petak_id };
        //     menu[0].group[0].children[0].children[1].params = { petakId: petak0.petak_id };
        //     menu[0].group[0].children[0].children[2].params = { petakId: petak0.petak_id };
        //     menu[0].group[0].children[0].children[3].params = { petakId: petak0.petak_id, name: petak0.name };

        //     /** petak 2 */
        //     menu[0].group[0].children[1].title = this.constructor._titleFormater(petak1.name);
        //     menu[0].group[0].children[1].params = petak1;

        //     menu[0].group[0].children[1].children[0].params = { petakId: petak1.petak_id };
        //     menu[0].group[0].children[1].children[1].params = { petakId: petak1.petak_id };
        //     menu[0].group[0].children[1].children[2].params = { petakId: petak1.petak_id };
        //     menu[0].group[0].children[1].children[3].params = { petakId: petak1.petak_id, name: petak1.name };

        //     /** mangrove */
        //     menu[0].group[0].children[2].title = this.constructor._titleFormater(petak2.name);
        //     menu[0].group[0].children[2].params = petak2;

        //     menu[0].group[0].children[2].children[0].params = { petakId: petak2.petak_id };
        //     menu[0].group[0].children[2].children[1].params = { petakId: petak2.petak_id };
        //     menu[0].group[0].children[2].children[2].params = { petakId: petak2.petak_id };
        //     menu[0].group[0].children[2].children[3].params = { petakId: petak2.petak_id, name: petak2.name };

        //     this.setState({ menu });
        // }
    }

    _navigationHandler(route, params) {
        const { navigation } = this.props;

        if (params) {
            if (Object.prototype.hasOwnProperty.call(params, 'petakId')) {
                const { petakId } = params;

                if (route === 'UpdateProgress') {
                    this.props.actions.getTask({ petakId })
                        .then((resolve) => {
                            navigation.navigate(route, { data: resolve, petakId, routeFrom: 'drawer' });
                        })
                        .catch((e) => {
                            let error = 'Unknown Error';

                            if ('message' in e) {
                                error = e.message;
                            }

                            alert(error);
                        });
                } else if (route === 'WaterQualityReport' || route === 'ShrimpReport') {
                    this.props.actions.getChart({ petak_id: petakId })
                        .then((resolve) => {
                            navigation.navigate(route, { data: resolve, routeFrom: 'drawer' });
                        })
                        .catch((e) => {
                            let error = 'Unknown Error';

                            if ('message' in e) {
                                error = e.message;
                            }

                            alert(error);
                        });
                } else if (route === 'MangroveGrowth') {
                    this.props.actions.getChart({ petak_id: petakId })
                        .then((resolve) => {
                            navigation.navigate(route, { data: resolve, routeFrom: 'drawer' });
                        })
                        .catch((e) => {
                            let error = 'Unknown Error';

                            if ('message' in e) {
                                error = e.message;
                            }

                            alert(error);
                        });
                } else if (route === 'MangroveNote') {
                    this.props.actions.getNote({ petakId })
                        .then((resolve) => {
                            navigation.navigate(route, { data: resolve, petakId, routeFrom: 'drawer' });
                        })
                        .catch((e) => {
                            let error = 'Unknown Error';

                            if ('message' in e) {
                                error = e.message;
                            }

                            alert(error);
                        });
                } else if (route === 'PictureGallery') {
                    this.props.actions.getImage({ petakId })
                        .then((resolve) => {
                            navigation.navigate(route, { data: resolve, ...(params || {}), routeFrom: 'drawer' });
                        })
                        .catch((e) => {
                            let error = 'Unknown Error';

                            if ('message' in e) {
                                error = e.message;
                            }

                            alert(error);
                        });
                }
            } else {
                navigation.navigate(route, params);
            }
        } else if (route) {
            navigation.navigate(route);
        }
    }

    _renderMenu({
        data, leftIndent, isGroup = false, isChild = false, childLevel = 0,
    }) {
        const { navigation } = this.props;

        const items = data.map((item, index) => {
            const leftStart = isGroup ? 40 : 15;
            const indent = leftIndent > 0 ? leftIndent + leftStart : 13;
            let textStyle = 'default';

            if (isChild) {
                textStyle = 'small';

                if (childLevel === 1) {
                    textStyle = 'medium';
                }
            }

            return (
                <View
                    key={item.id}
                    style={(item.routeTo === 'ChangePassword' ? { marginBottom: 30 } : {})}
                >
                    <DrawerItem
                        title={item.title}
                        {...(item.icon ? { icon: item.icon } : {}) }
                        indent={indent}
                        textType={textStyle}
                        pressEvent={() => this._navigationHandler(item.routeTo, Object.prototype.hasOwnProperty.call(item, 'params') ? item.params : null)}
                    />
                    {'group' in item &&
                        this._renderMenu({ data: item.group, leftIndent: indent, isGroup: true })
                    }
                    {'children' in item &&
                        this._renderMenu({
                            data: item.children, leftIndent: indent, isChild: true, childLevel: childLevel + 1,
                        })
                    }
                </View>
            );
        });

        return items;
    }

    render() {
        const { menu } = this.state;
        return (
            <ThemeProvider uiTheme={uiTheme}>
                <Container>
                    <StatusBar backgroundColor="rgba(21, 75, 120, 0.8)" />
                    <Toolbar
                        leftElement="arrow-back"
                        onLeftElementPress={() => this.props.navigation.navigate('DrawerClose')}
                        centerElement="Menu"
                    />
                    <View style={styles.container}>
                        <Drawer
                            style={{
                                container: {
                                    backgroundColor: '#F5F5F3',
                                },
                            }}
                        >
                            <View>
                                <View style={styles.profileContainer}>
                                    <View style={styles.profile}>
                                        <Image source={Logo} style={styles.profileImage} />
                                        <View style={styles.profileInfo}>
                                            <Text style={styles.titleTop}>Muara Gembong</Text>
                                            <Text style={styles.titleBottom}>Mandiri</Text>
                                        </View>
                                    </View>
                                </View>
                                {
                                    this._renderMenu({ data: menu, leftIndent: 0 })
                                }
                            </View>
                        </Drawer>
                    </View>
                </Container>
            </ThemeProvider>
        );
    }
}
