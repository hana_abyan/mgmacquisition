import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    processColor,
} from 'react-native';

import { LineChart } from 'react-native-charts-wrapper';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 400,
    },
    chart: {
        flex: 1,
    },
});

const initialChartDataset = {
    dataSets: [{
        // values: [{ y: 90 }, { y: 130 }, { y: 100 }, { y: 105 }],
        values: [90, 130, 100, 105],
        label: 'Company Y',
        config: {
            lineWidth: 9,
            // drawCubicIntensity: 0.4,
            circleRadius: 13,
            drawHighlightIndicators: false,
            color: processColor('#185188'),
            drawFilled: false,
            fillColor: processColor('#21A1E2'),
            // fillAlpha: 45,
            circleColor: processColor('#21A1E2'),
        },
    }],
};

const defaultXAxis = {
    position: 'BOTTOM',
    valueFormatter: ['W1', 'W2', 'W3', 'W4'],
};

const defaultDataSet = {
    values: [1, 2, 3, 4],
    config: {
        lineWidth: 1,
        drawHighlightIndicators: false,
        drawFilled: false,
    },
};

const defaultConf = {
    legend: {
        enabled: false,
    },
    marker: {
        enabled: false,
        digits: 2,
        markerColor: processColor('#F0C0FF8C'),
        textColor: processColor('white'),
    },
};

const LineChartScreen = ({ data, label, color, series }) => {
    const dataSets = data.filter(item => item.length > 0).map((item, index) => {
        const ds = JSON.parse(JSON.stringify(defaultDataSet));

        Object.assign(ds, {
            values: [...item],
            label: series[index],
            config: {
                ...ds.config,
                color: processColor(color[index]),
                circleRadius: 0,
                circleColor: processColor('transparent'),
                fillColor: processColor(color[index]),
                drawCircles: false,
            },
        });

        if (index === 0) {
            Object.assign(ds, {
                config: {
                    ...ds.config,
                    lineWidth: 9,
                    circleRadius: 13,
                    circleColor: processColor('#21A1E2'),
                    drawCircles: true,
                },
            });
        }

        return ds;
    });

    const xAxis = Object.assign({}, defaultXAxis, { valueFormatter: [...label] });

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.container}>
                <LineChart
                    style={styles.chart}
                    data={{ dataSets: JSON.parse(JSON.stringify(dataSets)) }}
                    chartDescription={{ text: '' }}
                    legend={defaultConf.legend}
                    marker={defaultConf.marker}
                    xAxis={xAxis}
                    drawGridBackground={false}
                    borderColor={processColor('#185188')}
                    borderWidth={0}
                    drawBorders
                    yAxis={{ right: { enabled: false } }}
                    touchEnabled
                    dragEnabled
                    scaleEnabled
                    scaleXEnabled
                    scaleYEnabled
                    pinchZoom
                    doubleTapToZoomEnabled
                    dragDecelerationEnabled
                    dragDecelerationFrictionCoef={0.99}
                    keepPositionOnRotation={false}
                />
            </View>
        </View>
    );
};

export default LineChartScreen;
