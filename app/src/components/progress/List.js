import React from 'react';
import { StyleSheet, View, Text, Button, Alert, TouchableWithoutFeedback, Image } from 'react-native';
// import CheckBox from 'react-native-check-box';
import CheckBox from '../../libraries/react-native-check-box';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
        padding: 15,
    },
    titleContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    boxTitle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        color: '#1A5185',
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxNumber: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#1A5185',
    },
    boxIcon: {
        fontWeight: 'bold',
        color: '#1A5185',
        fontSize: 25,
        marginTop: 5,
    },
    checkboxItem: {
        marginTop: 10,
    },
    statusContainer: {
        paddingLeft: 35,
        paddingRight: 35,
        marginTop: 10,
        marginBottom: 10,
    },
    statusTextBold: {
        fontWeight: 'bold',
        fontFamily: 'MyriadProBold',
    },
    statusTextBlue: {
        color: '#1A5185',
        fontFamily: 'MyriadProRegular',
    },
    statusTextFlexRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    checkboxItemTextLineTrough: {
        color: 'red',
    },
});

const taskDone = require('../../assets/images/icon_done.png');

const ProgressList = ({ number, name, data, isOpen, openHandler, progressValue, isDone, dueDate, changeEvent }) => {
    let icon,
        progressStatus;

    if (isOpen) {
        icon = (
            <Icon
                style={styles.boxIcon}
                name="ios-arrow-down-outline"
                color="#154B78"
            />
        );
    } else {
        icon = (
            <Icon
                style={styles.boxIcon}
                name="ios-arrow-forward-outline"
                color="#154B78"
            />
        );
    }

    if (isDone === '1' || progressValue === 100) {
        progressStatus = (
            <View style={styles.statusContainer}>
                <Text style={styles.statusTextBlue}>DONE</Text>
            </View>
        );

        icon = (
            <Image
                source={taskDone}
                style={{
                    marginTop: 5,
                    width: 25,
                    height: 25,
                }}
            />
        );
    } else {
        progressStatus = (
            <View style={styles.statusContainer}>
                <View style={styles.statusTextFlexRow}>
                    <Text style={[styles.statusTextBold, styles.statusTextBlue]}>Progress: </Text>
                    <Text style={[styles.statusTextBlue]}>{`${progressValue}%`}</Text>
                </View>
                <View style={styles.statusTextFlexRow}>
                    <Text style={[styles.statusTextBold, styles.statusTextBlue]}>Due date: </Text>
                    <Text style={styles.statusTextBlue}>{dueDate}</Text>
                </View>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <TouchableWithoutFeedback
                onPress={() => {
                    openHandler(number);
                }}
            >
            <View
                style={styles.titleContainer}
            >
                <Text style={styles.boxNumber}>{number + 1}</Text>
                <Text style={styles.boxTitle}>{name}</Text>
                {icon}
            </View>
            </TouchableWithoutFeedback>

            {progressStatus}

            {
                isOpen && data && data.length > 0 &&
                data.map(item => (
                    <View
                        key={item.taskdetail_id}
                        style={styles.checkboxItem}
                    >
                        <CheckBox
                            rightTextStyle={
                                item.progress === 100 ? {
                                    textDecorationLine: 'line-through',
                                } : {}
                            }
                            isChecked={item.progress === 100}
                            rightText={item.name}
                            onClick={() => changeEvent(item.taskdetail_id, number)}
                            disabled={item.progress === 100}
                        />
                    </View>
                ))
            }
        </View>
    );
};

export default ProgressList;
