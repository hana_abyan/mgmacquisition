import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

const Thread = ({ title, creator, createDate }) => (
    <View>
        <View>
            <Text>{title}</Text>
        </View>
        <View>
            Dimulai oleh: {creator}, pada {createDate}
        </View>
    </View>
);

Thread.propTypes = {
    title: PropTypes.string.isRequired,
    creator: PropTypes.string,
    createDate: PropTypes.string,
};

Thread.defaultProps = {
    creator: '',
    createDate: '',
};

export default Thread;
