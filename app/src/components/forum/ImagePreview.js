import React from 'react';
import { Image, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 100,
    },
});

const ImagePreview = ({ url, style, pressHandler }) => (
    <TouchableOpacity
        style={style}
        onPress={() => {
            Alert.alert('Konfirmasi', 'Hapus gambar?', [
                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                { text: 'OK', onPress: () => pressHandler() },
            ]);
        }}
    >
        <Image
            resizeMode={Image.resizeMode.cover}
            style={[styles.image]}
            source={{ uri: url }}
        />
    </TouchableOpacity>
);

ImagePreview.propTypes = {
    url: PropTypes.string.isRequired,
    style: PropTypes.shape({}),
    pressHandler: PropTypes.func,
};

ImagePreview.defaultProps = {
    style: {},
    pressHandler: () => { },
};

export default ImagePreview;
