import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    imageItem: {
        marginRight: 5,
    },
});

const ListImage = ({ data, showImageHandler }) => {
    return (
        <View
            style={styles.container}
        >
            {
                data.length > 0 &&
                data.map(item => (
                        <Icon
                            // key={item.id}
                            key={item.name || item.image_name}
                            name="file-image-o"
                            size={30}
                            color="#676767"
                            style={styles.imageItem}
                            onPress={() => showImageHandler(item.name || item.image_name)}
                        />
                ))
            }
        </View>
    );
};

export default ListImage;
