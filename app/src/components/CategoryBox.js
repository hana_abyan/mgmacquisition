import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    tagStyle: {
        backgroundColor: 'white',
        marginTop: 10,
        marginRight: 10,
        alignItems: 'center',
        flexDirection: 'row',
        paddingTop: 7,
        paddingBottom: 7,
        paddingLeft: 17,
        paddingRight: 17,
    },
    text: {
        // alignItems: 'center',
        fontSize: 17,
        fontFamily: 'MyriadProBold',
    },
});

const CategoryBox = ({ data, activeId, pressEvent }) => {
    return (
        <View style={styles.container}>
            {data.length > 0 &&
                data.map(item => (
                    <TouchableOpacity
                        key={item.id}
                        style={[styles.tagStyle, (activeId === item.id ? { backgroundColor: '#155386' } : {})]}
                        onPress={() => pressEvent(item.id)}
                        activeOpacity={0.7}
                    >
                        <Text style={[styles.text, (activeId === item.id ? { color: 'white' } : {})]}>{item.name}</Text>
                    </TouchableOpacity>
                ))
            }
        </View>
    );
};

export default CategoryBox;
