import React from 'react';
import { PropTypes } from 'prop-types';
import { StyleSheet, TouchableOpacity, Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
    },
    buttonStyle: {
        height: 80,
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,
        flex: 1,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#E4E4E4',
    },
    textButtonStyle: {
        color: '#154B78',
        fontSize: 19,
        fontWeight: '600',
        paddingLeft: 20,
        // paddingRight: 15,
        fontFamily: 'MyriadProBold',
        flex: 1,
        textAlign: 'center',
        flexGrow: 1,
    },
});

const ButtonWhite = ({
    text, pressEvent, noArrowRight, icon, textAlign,
}) => (
    <View style={styles.buttonContainer}>
        <TouchableOpacity
            style={styles.buttonStyle}
            onPress={e => pressEvent(e)}
            activeOpacity={0.7}
        >
            <View>
                {icon &&
                <Image source={icon} style={{ width: 40, height: 40, marginLeft: 20 }} />}
            </View>
            <Text
                style={[styles.textButtonStyle, { textAlign }]}
            >{text}
            </Text>
            <Icon
                name="ios-arrow-forward-outline"
                size={25}
                color={noArrowRight ? 'transparent' : '#154B78'}
                style={[{ marginRight: 20, fontWeight: 'bold' }]}
            />
        </TouchableOpacity>
    </View>
);

ButtonWhite.propTypes = {
    text: PropTypes.string.isRequired,
    pressEvent: PropTypes.func,
    noArrowRight: PropTypes.bool,
    icon: PropTypes.number,
    textAlign: PropTypes.string,
};

ButtonWhite.defaultProps = {
    pressEvent: () => {},
    noArrowRight: false,
    icon: null,
    textAlign: 'center',
};

export default ButtonWhite;
