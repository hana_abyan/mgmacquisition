import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    no: {
        fontSize: 16,
        padding: 6,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: '#E7E7E7',
        fontFamily: 'MyriadProRegular',
        width: 40,
    },
    keterangan: {
        flex: 1,
        fontSize: 16,
        padding: 6,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: '#E7E7E7',
        fontFamily: 'MyriadProRegular',
    },
    jumlah: {
        fontSize: 16,
        padding: 6,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: '#E7E7E7',
        fontFamily: 'MyriadProRegular',
    },
});

const MangroveNoteRow = ({ header, no, keterangan, jumlah }) => (
    <View style={styles.row}>
        <Text style={[styles.no, (header ? { fontFamily: 'MyriadProBold' } : {})]}>{no}</Text>
        <Text style={[styles.keterangan, (header ? { fontFamily: 'MyriadProBold' } : {})]}>{keterangan}</Text>
        <Text style={[styles.jumlah, (header ? { fontFamily: 'MyriadProBold' } : {})]}>{jumlah}</Text>
    </View>
);

MangroveNoteRow.propTypes = {
    header: PropTypes.bool,
    no: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]).isRequired,
    keterangan: PropTypes.string.isRequired,
    jumlah: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]).isRequired,
};

MangroveNoteRow.defaultProps = {
    header: false,
};

export default MangroveNoteRow;
