import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import LineChartScreen from '../LineChartScreen';
import CustomPicker from '../CustomPicker';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        paddingTop: 30,
        paddingBottom: 30,
        paddingLeft: 20,
        paddingRight: 20,
    },
    textTitle: {
        color: '#154B78',
        fontSize: 21,
        fontWeight: '700',
        marginTop: 20,
        fontFamily: 'MyriadProBold',
    },
    textInfo: {
        fontSize: 18,
        marginTop: 10,
        fontFamily: 'MyriadProRegular',
    },
    pickerContainer: {
        borderWidth: 1,
        borderColor: '#E3E3E3',
        marginBottom: 15,
    },
});

const ChartSection = ({ title, period, summary, data, label, color, series, periodValue, periodOptions, periodChangeHandler }) => {
    const customOptions = periodOptions.length > 0 ? periodOptions.map(item => ({
        id: `${item.chart_id}${item.year}${item.month}`,
        name: `${item.month_view} ${item.year}`,
        value: `${item.chart_id}-${item.year}-${item.month}`,
    })) : [];

    return (
        <View style={styles.container}>
            <View style={styles.pickerContainer}>
                <CustomPicker
                    value={periodValue}
                    data={customOptions}
                    changeEvent={val => periodChangeHandler(val)}
                    defaultEmptyLabel="-- Pilih Periode --"
                />
            </View>

            <LineChartScreen
                data={data}
                label={label}
                color={color}
                series={series}
            />
            <Text style={styles.textTitle}>{title}</Text>
            <Text style={styles.textTitle}>Periode Laporan</Text>
            <Text style={styles.textInfo}>{period}</Text>
            <Text style={styles.textTitle}>Ringkasan Laporan</Text>
            <Text style={styles.textInfo}>{summary}</Text>
        </View>
    )
};

export default ChartSection;
