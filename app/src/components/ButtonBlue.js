import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
    },
    buttonStyle: {
        height: 45,
        backgroundColor: '#2E7BB3',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        flex: 1,
    },
    textButtonStyle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'MyriadProRegular',
    },
});

const ButtonBlue = ({ text, pressEvent, dark, isLoading }) => (
    <View style={styles.buttonContainer}>
        <TouchableOpacity
            style={[styles.buttonStyle, (dark ? { backgroundColor: '#084071' } : {})]}
            onPress={e => pressEvent(e)}
            activeOpacity={0.7}
            disabled={isLoading}
        >
            {isLoading &&
                <ActivityIndicator size="large" color="#fff" />
            }

            {!isLoading &&
                <Text style={styles.textButtonStyle}>{text}</Text>
            }
        </TouchableOpacity>
    </View>
);

ButtonBlue.propTypes = {
    text: PropTypes.string.isRequired,
    pressEvent: PropTypes.func,
    dark: PropTypes.bool,
    isLoading: PropTypes.bool,
};

ButtonBlue.defaultProps = {
    pressEvent: () => {},
    dark: false,
    isLoading: false,
};

export default ButtonBlue;
