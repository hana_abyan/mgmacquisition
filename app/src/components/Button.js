import React, { Component } from 'react';
import Dimensions from 'Dimensions';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

export default class Button extends Component {
    render() {
        const { pressEvent, children, style } = this.props;
        return (
            <TouchableOpacity
                onPress={e => pressEvent(e)}
                style={[styles.button, style]}
            >
                {children}
            </TouchableOpacity>
        )
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
    button: {
        height: 40,
        backgroundColor: '#2E7BB3',
        width: DEVICE_WIDTH - 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
});