import React from 'react';

const test = () => (
    <LineChart
        style={styles.chartM}
        data={this.getWeekData(false)}
        description={"Week Wise"}
        visibleXRange={[0, 30]}
        maxVisibleValueCount={50}
        xAxis={{
            drawGridLines: false,
            gridLineWidth: 1,
            position: "BOTTOM",
            spaceBetweenLabels: 0,
            labelRotationAngle: -30.0,
            limitLines: [{
                limit: 115,
                lineColor: processColor('red'),
                lineWidth: 2
            }]
        }}
        yAxisRight={{ enable: false }}
        yAxis={{
            startAtZero: false,
            drawGridLines: false,
            position: "INSIDE_CHART"
        }}
        drawGridBackground={true}
        backgroundColor={"#f2f2f2"}
        legend={{
            enable: true,
            position: 'ABOVE_CHART_RIGHT',
            direction: "LEFT_TO_RIGHT"
        }}
        gridBackgroundColor={"#f2f2f2"}
        scaleEnabled={false} />
);
