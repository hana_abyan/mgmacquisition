import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

// const styles = StyleSheet.create({

// });

const tagColor = isActive => ({
    backgroundColor: isActive ? '#155386' : 'white',
});

const textColor = isActive => ({
    color: isActive ? 'white' : '#A1A1A1',
});

const CategoryTag = ({ name, isActive }) => (
    <TouchableOpacity
        style={[tagColor(isActive)]}
    >
        <Text
            style={[textColor(isActive)]}
        >{name}
        </Text>
    </TouchableOpacity>
);

CategoryTag.propTypes = {
    name: PropTypes.string.isRequired,
    isActive: PropTypes.bool,
};

CategoryTag.defaultProps = {
    isActive: false,
};

export default CategoryTag;
