import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    processColor,
} from 'react-native';

import { LineChart } from 'react-native-charts-wrapper';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#F5FCFF',
        height: 400,
    },
    chart: {
        flex: 1,
    },
});

const initialChartDataset = {
    dataSets: [{
        // values: [{ y: 90 }, { y: 130 }, { y: 100 }, { y: 105 }],
        values: [90, 130, 100, 105],
        label: 'Company Y',
        config: {
            lineWidth: 9,
            drawCubicIntensity: 0.4,
            circleRadius: 13,
            drawHighlightIndicators: false,
            color: processColor('#185188'),
            drawFilled: false,
            fillColor: processColor('#21A1E2'),
            // fillAlpha: 45,
            circleColor: processColor('#21A1E2'),
        },
    }],
};

const initialChartXAxis = {
    position: 'BOTTOM',
    valueFormatter: ['W1', 'W2', 'W3', 'W4'],
};

export default class LineChartScreen extends React.Component {
    constructor() {
        super();

        this.state = {
            data: {},
            legend: {
                enabled: false,
            //     textColor: processColor('blue'),
            //     textSize: 12,
            //     position: 'BELOW_CHART_RIGHT',
            //     form: 'SQUARE',
            //     formSize: 14,
            //     xEntrySpace: 10,
            //     yEntrySpace: 5,
            //     formToTextSpace: 5,
            //     wordWrapEnabled: true,
            //     maxSizePercent: 0.5,
            //     custom: {
            //         colors: [processColor('red'), processColor('blue'), processColor('green')],
            //         labels: ['Company X', 'Company Y', 'Company Dashed']
            //     },
            },
            marker: {
                enabled: false,
                digits: 2,
                // backgroundTint: processColor('teal'),
                markerColor: processColor('#F0C0FF8C'),
                textColor: processColor('white'),
            },
        };
    }

    componentWillMount() {
        const { data: { x, y } } = this.props;
        const initData = JSON.parse(JSON.stringify(initialChartDataset));
        const initXAxis = JSON.parse(JSON.stringify(initialChartXAxis));
        if (this.props.data) {
            initData.dataSets[0].values = y;
            initXAxis.valueFormatter = x;
        }
        this.setState({
            data: initData,
            xAxis: initXAxis,
        });
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }

    handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({ ...this.state, selectedEntry: null })
        } else {
            this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
        }

        console.log(event.nativeEvent)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.container}>
                    <LineChart
                        style={styles.chart}
                        data={this.state.data}
                        chartDescription={{ text: '' }}
                        legend={this.state.legend}
                        marker={this.state.marker}
                        xAxis={this.state.xAxis}
                        drawGridBackground={false}
                        borderColor={processColor('#185188')}
                        borderWidth={0}
                        drawBorders={true}

                        yAxis={{ right: { enabled: false } }}

                        touchEnabled={true}
                        dragEnabled={true}
                        scaleEnabled={true}
                        scaleXEnabled={true}
                        scaleYEnabled={true}
                        pinchZoom={true}
                        doubleTapToZoomEnabled={true}

                        dragDecelerationEnabled={true}
                        dragDecelerationFrictionCoef={0.99}

                        keepPositionOnRotation={false}
                        onSelect={this.handleSelect.bind(this)}
                        onChange={(event) => console.log(event.nativeEvent)}
                    />
                </View>

            </View>
        );
    }
}
