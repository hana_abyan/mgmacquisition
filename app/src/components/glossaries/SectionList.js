import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';

const styles = StyleSheet.create({
    container: {
        marginTop: 18,
        paddingBottom: 20,
    },
    title: {
        color: '#155386',
        fontSize: 32,
        fontFamily: 'MyriadProRegular',
        fontWeight: '500',
        marginBottom: 25,
    },
    item: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: 10,
    },
    bullet: {
        color: '#155386',
        fontSize: 19,
        fontFamily: 'MyriadProRegular',
    },
    term: {
        flex: 1,
        color: '#155386',
        fontSize: 17,
        fontFamily: 'MyriadProRegular',
        fontWeight: '500',
        paddingLeft: 10,
    },
    separator: {
        color: '#7C7C7C',
        fontSize: 17,
        fontFamily: 'MyriadProRegular',
        fontWeight: '500',
    },
    description: {
        flex: 4,
        color: '#7C7C7C',
        fontSize: 17,
        fontFamily: 'MyriadProRegular',
        fontWeight: '500',
    },
});

const SectionList = ({ header, data }) => (
    <View
        style={styles.container}
    >
        <Text
            style={styles.title}
        >{header}
        </Text>

        {data.length > 0 &&
            data.map(item => (
                <View
                    key={item.id}
                    style={styles.item}
                >
                    <Text style={styles.bullet}>{`\u2022`}</Text>
                    <Text style={styles.term}>{item.istilah}</Text>
                    <Text style={styles.separator}>: </Text>
                    <Text style={styles.description}>{item.arti}</Text>
                </View>
            ))
        }
    </View>
);

SectionList.propTypes = {
    header: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(PropTypes.shape({})),
};

SectionList.defaultProps = {
    data: [],
};

export default SectionList;
