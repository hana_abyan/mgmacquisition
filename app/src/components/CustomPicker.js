import React from 'react';
import { StyleSheet, Picker } from 'react-native';

const styles = StyleSheet.create({
    picker: {
        backgroundColor: 'white',
        height: 40,
    },
    pickerItem: {
        fontFamily: 'MyriadProRegular',
    },
});

const CustomPicker = ({ defaultEmptyLabel, value, data, changeEvent }) => {
    const pickerItem = [];
    if (defaultEmptyLabel && defaultEmptyLabel !== '') {
        pickerItem.push(<Picker.Item key="none" label={defaultEmptyLabel} value={null} style={styles.pickerItem} />);
    }
    if (data.length > 0) {
        data.map((item) => {
            pickerItem.push(<Picker.Item key={item.id} label={item.name} value={item.value} style={styles.pickerItem} />);
        });
    }

    return (
        <Picker
            style={styles.picker}
            selectedValue={value}
            onValueChange={val => changeEvent(val)}
        >
            {pickerItem}
        </Picker>
    );
};

export default CustomPicker;
