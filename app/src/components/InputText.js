import React from 'react';
import { View, TextInput, StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
    inputWrapper: {
        flexDirection: 'row',
    },
    input: {
        flex: 1,
        flexDirection: 'row',
        height: 40,
        marginTop: 10,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#E3E3E3',
        backgroundColor: '#fff',
        color: '#777',
        paddingLeft: 20,
        paddingRight: 20,
        fontFamily: 'MyriadProRegular',
    },
});

const InputText = ({
    placeholder, value, changeEvent, customProps, label, setRef, submitEditingHandler,
}) => (
    <View style={styles.inputWrapper}>
        {label &&
            <View>
                <Text>{label}</Text>
            </View>
        }
        <TextInput
            placeholder={placeholder}
            onChangeText={text => changeEvent(text)}
            value={`${value}`}
            style={styles.input}
            underlineColorAndroid="transparent"
            disableFullscreenUI
            {...customProps}
            ref={setRef}
            onSubmitEditing={() => submitEditingHandler()}
        />
    </View>
);

InputText.propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    changeEvent: PropTypes.func,
    customProps: PropTypes.shape({}),
    label: PropTypes.string,
    setRef: PropTypes.func,
    submitEditingHandler: PropTypes.func,
};

InputText.defaultProps = {
    placeholder: '',
    value: '',
    changeEvent: () => {},
    customProps: {},
    label: null,
    setRef: () => {},
    submitEditingHandler: () => {},
};

export default InputText;
