import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { StyleSheet, View, ScrollView, Text, Alert, TextInput, TouchableOpacity, Image, TouchableWithoutFeedback, Keyboard } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';

import ContainerHOC from '../../core/ContainerHOC';
import CustomPicker from '../../components/CustomPicker';
import ButtonBlue from '../../components/ButtonBlue';
import Message from '../../components/Message';
import ImagePreview from '../../components/forum/ImagePreview';

import * as forumsActionCreators from '../../data/forums/actions';
import * as forums from '../../data/forums';

const styles = StyleSheet.create({
    scroll: {
        marginTop: 5,
        flexDirection: 'column',
    },
    container: {
        marginTop: 20,
        paddingLeft: 15,
        paddingRight: 15,
    },
    inner: {
        backgroundColor: '#fff',
        paddingLeft: 25,
        paddingRight: 25,
        // paddingTop: 20,
        paddingBottom: 20,
    },
    formGroup: {
        flexDirection: 'column',
        marginTop: 15,
    },
    controlLabel: {
        flex: 1,
        color: '#084071',
        fontFamily: 'MyriadProRegular',
        fontSize: 22,
    },
    formControl: {
        flex: 1,
        flexDirection: 'row',
        height: 40,
        marginTop: 10,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#E3E3E3',
        backgroundColor: '#fff',
        color: '#777',
        paddingLeft: 20,
        paddingRight: 20,
        fontFamily: 'MyriadProRegular',
    },
    formControlTextArea: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#E3E3E3',
        backgroundColor: '#fff',
        color: '#777',
        paddingLeft: 20,
        paddingRight: 20,
        fontFamily: 'MyriadProRegular',
        textAlignVertical: 'top',
    },
    messageContainer: {
        marginTop: 10,
    },
    buttonWrapper: {
        marginTop: 10,
    },
    pickerContainer: {
        borderWidth: 1,
        borderColor: '#E3E3E3',
        marginTop: 10,
    },
    imagePreview: {
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
});

@connect(
    state => ({
        categories: state.forums.categories,
    }),
    dispatch => ({
        actions: bindActionCreators(forumsActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { center: 'Forum' } })
export default class ForumCreateThreadScreen extends Component {
    state = {
        category: this.props.navigation.state.params.categoryId,
        title: '',
        description: '',
        files: [],
        isLoading: false,
        message: '',
        status: false,
    }

    inputs = {}

    _submitHandler() {
        this.setState({ isLoading: true });
        const {
            category, title, description, files,
        } = this.state;

        if (String(category).length <= 0 || String(title).length <= 0 || String(description).length <= 0) {
            this.setState({ status: false, message: 'Isikan kategori, judul, dan pesan.', isLoading: false });
        } else {
            let images = [];

            if (files.length > 0) {
                images = files.map((item, index) => ({
                    ...item,
                    name: `images[${index}]`,
                    data: RNFetchBlob.wrap(item.data),
                }));
            }
            // const data = {
            //     filename: 'image.jpg',
            //     type: 'image/jpg',
            //     data: RNFetchBlob.wrap(file),
            // };
            // forums.postThread({
            //     forum_category_id: category,
            //     title,
            //     text: description,
            // })

            forums.postThread([
                { name: 'forum_category_id', data: `${category}` },
                { name: 'title', data: title },
                { name: 'text', data: description },
                ...images,
            ])
                .then((resolve) => {
                    const resData = typeof resolve.data === 'string' ? JSON.parse(resolve.data) : resolve.data;

                    if (resData.status.toLowerCase() === 'ok') {
                        this.props.actions.getThread({ categoryId: category });
                        this.setState({
                            message: resData.message,
                            status: true,
                            isLoading: false,
                            category: '',
                            title: '',
                            files: [],
                            description: '',
                        }, () => {
                            setTimeout(() => {
                                this.props.navigation.goBack(null);
                                this.props.navigation.state.params.goToPage('detail', resData.data.id, true);
                            }, 1000);
                        });
                    }
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e || 'data' in e || 'error' in e) {
                        error = e.message || e.data || e.error;
                    }

                    const message = typeof error === 'object' ? Object.values(error).reduce((prev, current) => [...prev, ...current], []) : error;

                    this.setState({ message, status: false, isLoading: false });
                });
        }
    }

    selectPhotoTapped() {
        const options = {
            takePhotoButtonTitle: '',
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                const files = [...this.state.files, { filename: response.fileName, type: response.type, data: response.uri }];

                this.setState({
                    // file: response.uri,
                    files,
                });
            }
        });
    }

    _setRef(key, com) {
        this.inputs[key] = com;
    }

    _focusNextField(id) {
        this.inputs[id].focus();
    }

    _deleteImageHandler(index) {
        const files = JSON.parse(JSON.stringify(this.state.files));

        files.splice(index, 1);

        this.setState({ files });
    }

    render() {
        const {
            category, title, description, files, isLoading, message, status,
        } = this.state,
            { categories } = this.props,
            customOptions = [];

        if (categories && categories.length > 0) {
            categories.map(item => customOptions.push({ id: item.category_forum_id, name: item.category_forum_name, value: item.category_forum_id }));
        }

        return (
            <View style={{ flex: 1 }}>
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag"
                >
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View
                            style={styles.container}
                        >
                            <View
                                style={styles.inner}
                            >
                                <View style={styles.formGroup}>
                                    <Text
                                        style={styles.controlLabel}
                                    >Kategori
                                    </Text>
                                    <View style={styles.pickerContainer}>
                                        <CustomPicker
                                            value={category}
                                            data={customOptions}
                                            changeEvent={val => this.setState({ category: val })}
                                            defaultEmptyLabel="-- Pilih Kategori --"
                                        />
                                    </View>
                                </View>

                                <View style={styles.formGroup}>
                                    <Text
                                        style={styles.controlLabel}
                                    >Judul
                                    </Text>
                                    <TextInput
                                        value={title}
                                        onChangeText={val => this.setState({ title: val })}
                                        style={styles.formControl}
                                        underlineColorAndroid="transparent"
                                        autoCorrect={false}
                                        returnKeyType="next"
                                        blurOnSubmit={false}
                                        ref={c => this._setRef('one', c)}
                                        onSubmitEditing={() => this._focusNextField('two')}
                                    />
                                </View>

                                <View style={styles.formGroup}>
                                    <Text
                                        style={styles.controlLabel}
                                    >Pesan
                                    </Text>
                                    <TextInput
                                        value={description}
                                        onChangeText={val => this.setState({ description: val })}
                                        style={styles.formControlTextArea}
                                        underlineColorAndroid="transparent"
                                        autoCorrect={false}
                                        returnKeyType="done"
                                        editable
                                        multiline
                                        numberOfLines={8}
                                        blurOnSubmit
                                        ref={c => this._setRef('two', c)}
                                    />
                                </View>

                                <View style={styles.formGroup}>
                                    <Text
                                        style={styles.controlLabel}
                                    >Lampirkan gambar (jika ada)
                                    </Text>
                                    <TouchableOpacity
                                        onPress={() => this.selectPhotoTapped()}
                                        style={{
                                            backgroundColor: '#EBEBEB',
                                            flex: 0,
                                            flexGrow: 0,
                                            width: 80,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            padding: 5,
                                            marginTop: 5,
                                        }}
                                    >
                                        <Text
                                            style={{
                                                color: '#929292',
                                                fontSize: 16,
                                            }}
                                        >Attach file
                                        </Text>

                                    </TouchableOpacity>
                                    {
                                        files.length > 0 &&
                                        <View
                                            style={styles.imagePreview}
                                        >
                                            {
                                                files.map((item, index) => (
                                                    <ImagePreview
                                                        key={item.filename}
                                                        url={item.data}
                                                        style={{ marginTop: 10, marginRight: 10 }}
                                                        pressHandler={() => this._deleteImageHandler(index)}
                                                    />
                                                ))
                                            }
                                        </View>
                                    }
                                </View>

                                {
                                    typeof message === 'string' && message !== '' &&
                                    <View style={styles.messageContainer}>
                                        <Message
                                            type={status ? 'success' : 'error'}
                                            message={message}
                                        />
                                    </View>
                                }

                                {
                                    typeof message === 'object' && message.length > 0 &&
                                    <View style={styles.messageContainer}>
                                        {
                                            message.map(item => (
                                                <Message
                                                    key={item}
                                                    type={status ? 'success' : 'error'}
                                                    message={item}
                                                />
                                            ))
                                        }
                                    </View>
                                }

                                <View
                                    style={styles.buttonWrapper}
                                >
                                    <ButtonBlue
                                        text="Simpan"
                                        pressEvent={() => {
                                            Alert.alert('Konfirmasi', 'Buat topik baru?', [
                                                { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                                { text: 'OK', onPress: () => this._submitHandler() },
                                            ]);
                                        }}
                                        isLoading={isLoading}
                                        dark
                                    />
                                </View>

                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </View>
        );
    }
}
