import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView, View, Text, StyleSheet, TextInput, Image, Alert, TouchableOpacity } from 'react-native';
import { Avatar } from 'react-native-material-ui';
import ImagePreview from 'react-native-image-preview';
// import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import ImagePicker from 'react-native-image-picker';

import ContainerHOC from '../../core/ContainerHOC';
import ButtonBlue from '../../components/ButtonBlue';
import Message from '../../components/Message';
import ListImage from '../../components/forum/ListImage';
import ImagePreviews from '../../components/forum/ImagePreview';

import * as forumsActionCreators from '../../data/forums/actions';
import * as forums from '../../data/forums';

const styles = StyleSheet.create({
    scroll: {
        paddingBottom: 30,
    },
    container: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
    },
    titleWrapper: {
        marginTop: 20,
    },
    headerTitleText: {
        color: '#154B78',
        fontSize: 25,
        fontWeight: '700',
        alignSelf: 'flex-start',
    },
    subTitle: {
        fontSize: 15,
        marginTop: 7,
    },
    forumDetail: {
        flexDirection: 'row',
        marginTop: 10,
        backgroundColor: '#fff',
    },
    detailContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        marginTop: 10,
    },
    subHeaderWrapper: {
        marginTop: 7,
    },
    replyWrapper: {
        marginTop: 30,
    },
    replyItem: {
        backgroundColor: '#fff',
        marginBottom: 10,
        padding: 10,
    },
    replyItemText: {
        fontSize: 13,
    },
    commentWrapper: {
        marginTop: 20,
        backgroundColor: '#fff',
        paddingTop: 12,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 25,
    },
    commentTitle: {
        color: '#154B78',
        fontSize: 18,
        fontWeight: '500',
    },
    input: {
        fontSize: 20,
        borderWidth: 1,
        borderColor: '#E3E3E3',
        backgroundColor: '#fff',
        color: '#777',
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 15,
        textAlignVertical: 'top',
    },
    buttonWrapper: {
        marginTop: 20,
    },
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderColor: '#eee',
        padding: 10,
    },
    mediaContent: {
        flex: 1,
        marginLeft: 20,
    },
    user: {
        color: '#154B78',
        fontSize: 15,
        fontWeight: '500',
    },
    message: {
        fontSize: 17,
        marginTop: 15,
    },
    postDate: {
        color: '#D0D0D0',
        fontSize: 15,
        marginTop: 17,
        paddingBottom: 15,
    },
    imageContainer: {
        borderTopWidth: 1,
        borderColor: '#eaeaea',
        marginTop: 15,
        paddingTop: 10,
        paddingBottom: 5,
    },
    imagePreview: {
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
});

const ForumMessage = ({
    image, name, message, postDate, replyImages, showImageHandler,
}) => {
    const img = (<Image source={{ uri: image }} style={{ width: 65, height: 65 }} />);
    return (
        <View
            style={[styles.wrapper]}
        >
            <View>
                <Avatar
                    image={img}
                    size={60}
                    style={{
                        container: {
                            overflow: 'hidden',
                            backgroundColor: '#F5F5F3',
                        },
                        content: {
                            borderWidth: 1,
                        },
                    }}
                />
            </View>
            <View
                style={styles.mediaContent}
            >
                <Text style={styles.user}>{name}</Text>
                <Text style={styles.message}>{message}</Text>
                {postDate &&
                    <Text style={styles.postDate}>{postDate}</Text>
                }

                {
                    replyImages.length > 0 &&
                    <View
                        style={styles.imageContainer}
                    >
                        <ListImage
                            data={replyImages}
                            showImageHandler={imageUrls => showImageHandler(imageUrls)}
                        />
                    </View>
                }
            </View>
        </View>
    );
};

@connect(
    state => ({
        detail: state.forums.detail,
    }),
    dispatch => ({
        actions: bindActionCreators(forumsActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { center: 'Forum' } })
export default class ForumDetailScreen extends Component {
    state = {
        comment: '',
        message: '',
        status: false,
        data: {},
        isLoading: false,
        showModal: false,
        imagesToShow: [],
        files: '',
    }

    componentWillMount() {
        const { navigation: { state } } = this.props;
        if ('params' in state && state.params && 'data' in state.params) {
            this.setState({ data: JSON.parse(JSON.stringify(state.params.data)) });
        }
        if ('params' in state && state.params && 'disableSplash' in state.params) {
            state.params.disableSplash();
        }
    }

    _replyForum() {
        const { data, comment, files } = this.state,
            threadId = data.thread_id;

        this.setState({ isLoading: true });

        if (comment && comment.length > 1) {
            // const images = {
            //     filename: 'image.jpg',
            //     type: 'image/jpg',
            //     data: RNFetchBlob.wrap(file),
            // };
            let images = [];

            if (files.length > 0) {
                images = files.map((item, index) => ({
                    ...item,
                    name: `images[${index}]`,
                    data: RNFetchBlob.wrap(item.data),
                }));
            }

            // forums.postReply({ threadId, text: comment })
            forums.postReply([
                { name: 'thread_id', data: `${threadId}` },
                { name: 'text', data: comment },
                ...images,
                // { name: 'images[0]', ...images },
            ])
                .then((resolve) => {
                    const resData = typeof resolve.data === 'string' ? JSON.parse(resolve.data) : resolve.data;

                    if (resData.status.toLowerCase() === 'ok') {
                        this.props.actions.getDetail({ threadId }).then(res => this.setState({ data: JSON.parse(JSON.stringify(res)) }));
                        this.setState({
                            comment: '',
                            message: 'Berhasil menambahkan komentar',
                            status: true,
                            files: [],
                        });
                    }
                    this.setState({ isLoading: false });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e || 'data' in e || 'error' in e) {
                        error = e.message || e.data || e.error;
                    }

                    const message = typeof error === 'object' ? Object.values(error).reduce((prev, current) => [...prev, ...current], []) : error;

                    this.setState({ message, status: false, isLoading: false });
                });
        } else {
            this.setState({ status: false, message: 'Isikan komentar anda', isLoading: false });
        }
    }

    _viewImageHandler(imageUrls) {
        this.setState({ showModal: true, imagesToShow: imageUrls });
    }

    selectPhotoTapped() {
        const options = {
            takePhotoButtonTitle: '',
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                const files = [...this.state.files, { filename: response.fileName, type: response.type, data: response.uri }];

                this.setState({
                    // file: response.uri,
                    files,
                });
            }
        });
    }

    _deleteImageHandler(index) {
        const files = JSON.parse(JSON.stringify(this.state.files));

        files.splice(index, 1);

        this.setState({ files });
    }

    render() {
        const {
            comment, message, status, data, isLoading, files, showModal, imagesToShow,
        } = this.state;

        let threadTitle = '',
            threadCreatedAt = '',
            list = [];

        if (Object.keys(data).length > 0) {
            threadTitle = data.thread_title;
            threadCreatedAt = data.thread_created_at;
            list = [{
                reply_id: `detail${data.thread_id}`,
                reply_text: data.thread_desc,
                reply_by: data.thread_by,
                reply_created_at: null,
                reply_profile_image: data.thread_profile_image,
                reply_images: data.thread_images,
                type: 'thread',
            }, ...data.reply];
        }

        return (
            <ScrollView style={styles.scroll}>
                <View style={styles.container}>
                    <View style={styles.titleWrapper}>
                        <Text style={styles.headerTitleText}>{threadTitle}</Text>
                        <Text style={styles.subTitle}>{threadCreatedAt}</Text>
                    </View>

                    <View style={styles.detailContainer}>
                        {list.length > 0 &&
                            list.map(item => (
                                <View
                                    key={`forum-detail-${item.reply_id}`}
                                    style={[styles.forumDetail,
                                        (item.type
                                            ? {
                                                marginBottom: 10,
                                                borderTopWidth: 5,
                                                borderColor: '#1A537C',
                                                borderTopLeftRadius: 5,
                                                borderTopRightRadius: 5,
                                            }
                                            : {}
                                        ),
                                    ]}
                                >
                                    <ForumMessage
                                        image={item.reply_profile_image}
                                        name={item.reply_by}
                                        message={item.reply_text}
                                        postDate={item.reply_created_at}
                                        replyImages={item.reply_images}
                                        showImageHandler={imageUrls => this._viewImageHandler(imageUrls)}
                                    />
                                </View>
                            ))
                        }
                    </View>

                    <View
                        style={styles.commentWrapper}
                    >
                        <Text style={styles.commentTitle}>Masukkan Komentar</Text>
                        <TextInput
                            style={styles.input}
                            value={comment}
                            onChangeText={text => this.setState({ comment: text })}
                            editable
                            multiline
                            numberOfLines={5}
                            underlineColorAndroid="transparent"
                            autoCorrect={false}
                            blurOnSubmit
                            returnKeyType="done"
                        />

                        <View>
                            <Text
                                style={[styles.commentTitle, { marginTop: 10 }]}
                            >Lampirkan gambar (jika ada)
                            </Text>
                            <TouchableOpacity
                                onPress={() => this.selectPhotoTapped()}
                                style={{
                                    backgroundColor: '#EBEBEB',
                                    flex: 0,
                                    flexGrow: 0,
                                    width: 80,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    padding: 5,
                                    marginTop: 5,
                                }}
                            >
                                <Text
                                    style={{
                                        color: '#929292',
                                        fontSize: 16,
                                    }}
                                >Attach file
                                </Text>

                            </TouchableOpacity>
                            {
                                files.length > 0 &&
                                <View
                                    style={styles.imagePreview}
                                >
                                    {
                                        files.map((item, index) => (
                                            <ImagePreviews
                                                key={item.filename}
                                                url={item.data}
                                                style={{ marginTop: 10, marginRight: 10 }}
                                                pressHandler={() => this._deleteImageHandler(index)}
                                            />
                                        ))
                                    }
                                </View>
                            }
                        </View>

                        {
                            typeof message === 'string' && message !== '' &&
                            <View style={styles.messageContainer}>
                                <Message
                                    type={status ? 'success' : 'error'}
                                    message={message}
                                />
                            </View>
                        }

                        {
                            typeof message === 'object' && message.length > 0 &&
                            <View style={styles.messageContainer}>
                                {
                                    message.map(item => (
                                        <Message
                                            key={item}
                                            type={status ? 'success' : 'error'}
                                            message={item}
                                        />
                                    ))
                                }
                            </View>
                        }
                        <View style={styles.buttonWrapper}>
                            <ButtonBlue
                                text="Kirim Komentar"
                                pressEvent={() => {
                                    Alert.alert('Konfirmasi', 'Kirim komentar?', [
                                        { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                        { text: 'OK', onPress: () => this._replyForum() },
                                    ]);
                                }}
                                isLoading={isLoading}
                            />
                        </View>
                    </View>

                    {/* <Modal
                        visible={showModal}
                        transparent
                        onRequestClose={() => this.setState({ showModal: false })}
                    >
                        <ImageViewer imageUrls={imagesToShow} />
                    </Modal> */}
                </View>
                <ImagePreview
                    visible={showModal}
                    source={{ uri: imagesToShow }}
                    close={() => this.setState({ showModal: false })}
                />
            </ScrollView>
        );
    }
}
