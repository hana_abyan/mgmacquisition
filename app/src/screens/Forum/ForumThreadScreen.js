import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import moment from 'moment';
import { ActionButton } from 'react-native-material-ui';

import ContainerHOC from '../../core/ContainerHOC';
import InputSearch from '../../components/InputSearch';
import SplashScreen from '../../screens/SplashScreen';

import * as forumsActionCreators from '../../data/forums/actions';
import * as forums from '../../data/forums';

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 60,
    },
    headingText: {
        fontSize: 35,
        color: '#676767',
    },
    headerTitleText: {
        color: '#154B78',
        fontSize: 24,
        fontWeight: '500',
        alignSelf: 'flex-start',
        marginTop: 15,
    },
    threadContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        marginTop: 10,
    },
    thread: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        backgroundColor: '#fff',
    },
    wrapper: {
        flex: 1,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#eee',
    },
    inner: {
        // flexDirection: 'row',
        // alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 10,
        paddingRight: 10,
    },
    title: {
        flex: 1,
        color: '#2C4D7D',
        fontSize: 21,
        fontWeight: '500',
    },
    subTitle: {
        fontSize: 15,
        marginTop: 7,
    },
});

const ForumThread = ({ title, createdBy, createdAt, pressEvent }) => (
    <TouchableOpacity
        style={styles.wrapper}
        onPress={() => pressEvent()}
        activeOpacity={0.5}
    >
        <View style={styles.inner}>
            <Text
                style={styles.title}
            >{title}
            </Text>
            <Text style={styles.subTitle}>Dimulai oleh: {createdBy}, pada {createdAt}</Text>
        </View>
    </TouchableOpacity>
);

@connect(
    state => ({
        threads: state.forums.threads,
    }),
    dispatch => ({
        actions: bindActionCreators(forumsActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { center: 'Forum' } })
export default class ForumThreadScreen extends Component {
    state = {
        searchKey: '',
        data: [],
        showSplash: false,
        message: '',
        isLoading: false,
    }

    componentWillMount() {
        // const categoryId = this.props.navigation.state.params.categoryId;
        // this.props.actions.emptyThread();
        // this.props.actions.getThread({ categoryId });
        // this.props.actions.getCategoryWithReplies({ categoryId });
        if ('params' in this.props.navigation.state && this.props.navigation.state.params) {
            const searchKey = 'searchKey' in this.props.navigation.state.params ? this.props.navigation.state.params.searchKey : '';
            this._setData(this.props.navigation.state.params.data, searchKey);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.threads !== nextProps.threads) {
            this.setState({ data: JSON.parse(JSON.stringify(nextProps.threads)) });
        }
    }

    _setData(list, searchKey) {
        const data = list.map(item => ({ ...item, createdBy: 'Abdulrohman', createdAt: moment(item.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY') }));
        this.setState({ data, searchKey });
    }

    _navigationHandler(type, threadId, showSplash = false) {
        const { navigation } = this.props,
        { categoryId } = navigation.state.params;

        if (showSplash) {
            this.setState({ showSplash });
        }

        if (type === 'detail') {
            this.props.actions.getDetail({ threadId })
            .then((resolve) => {
                navigation.navigate('ForumDetail', { data: resolve, ...(showSplash ? { disableSplash: () => this.setState({ showSplash: false }) } : {}) });
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                alert(error);
            });
        }

        if (type === 'create') {
            navigation.navigate('ForumThreadNew', { categoryId, goToPage: (screenType, id, isShowSplash) => this._navigationHandler(screenType, id, isShowSplash) });
        }
    }
    _searchHandler() {
        const { searchKey } = this.state,
        { navigation } = this.props;

        this.setState({ isLoading: true });

        forums.postSearch({ value: searchKey })
            .then((resolve) => {
                const payload = {
                    message: '',
                };

                if (resolve.length === 0) {
                    Object.assign(payload, { message: `Tidak dapat menemukan pencarian dengan kata kunci ${searchKey}` });
                } else {
                    navigation.navigate('ForumThread', { data: resolve, searchKey });
                }

                this.setState({ ...payload, isLoading: false });
                // navigation.navigate('ForumThread', { data: resolve, categoryId, categoryName });
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                this.setState({ isLoading: false });
                alert(error);
            });
    }

    _resetSearch() {
        this.setState({ searchKey: '', message: '' });
    }

    render() {
        const { navigation, navigation : { state } } = this.props,
        {
            searchKey, data, showSplash, isLoading, message,
        } = this.state;

        if (showSplash) {
            return <SplashScreen />;
        }

        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={styles.scroll}>
                    <View style={styles.container}>
                        <InputSearch
                            placeholder="Cari forum..."
                            changeEvent={val => this.setState({ searchKey: val })}
                            value={searchKey}
                            onSubmit={() => this._searchHandler()}
                            isLoading={isLoading}
                        />
                        {
                            message.length > 0 &&
                            <Text>{message}</Text>
                        }
                        <Text style={styles.headerTitleText}>{state.params.categoryName}</Text>

                        <View style={styles.threadContainer}>
                            {data.length > 0 &&
                                data.map(item => (
                                    <View
                                        style={styles.thread}
                                        key={`forum-thread-${item.thread_id}`}
                                    >
                                        <ForumThread
                                            title={item.thread_title}
                                            createdBy={item.thread_created_by}
                                            createdAt={item.thread_created_view}
                                            pressEvent={() => this._navigationHandler('detail', item.thread_id)}
                                        />
                                    </View>
                                ))
                            }
                        </View>
                    </View>
                </ScrollView>
                <ActionButton
                    icon="create"
                    style={{
                        container: {
                            backgroundColor: '#072B70', position: 'absolute', bottom: 5, right: 10,
                        },
                    }}
                    onPress={() => this._navigationHandler('create')}
                />
            </View>
        );
    }
}
