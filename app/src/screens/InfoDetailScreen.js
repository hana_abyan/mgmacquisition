import React from 'react';
import { StyleSheet, ScrollView, View, Image, Text, TouchableOpacity, WebView } from 'react-native';

import ContainerHOC from '../core/ContainerHOC';

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 30,
        paddingLeft: 10,
        paddingRight: 10,
    },
});

const InfoDetailScreen = ({ navigation, windowWidth, windowHeight }) => {
    const {
        uri,
    } = navigation.state.params;
    return (
        <View
            style={styles.container}
        >
            <WebView
                source={{ uri }}
                style={{
                    flex: 1,
                    borderWidth: 1,
                    width: windowWidth - 20,
                    height: windowHeight,
                }}
                javaScriptEnabled
                domStorageEnabled
                startInLoadingState
            />
        </View>
    );
};

export default ContainerHOC({ header: { center: 'Info' } })(InfoDetailScreen);
