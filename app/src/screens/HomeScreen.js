import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

import ContainerHOC from '../core/ContainerHOC';
import ButtonWhite from '../components/ButtonWhite';

import * as clustersActionCreators from '../data/clusters/actions';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 25,
        paddingRight: 25,
    },
    headingTextContainer: {
        marginBottom: 30,
    },
    headingText: {
        fontSize: 30,
        color: '#676767',
    },
});

@connect(
    state => ({
        menu: state.clusters.menu,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { left: 'menu', center: 'Beranda', destination: 'DrawerToggle' } })
export default class HomeScreen extends Component {
    static propTypes = {
        menu: PropTypes.arrayOf(PropTypes.shape({})),
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
        }),
    }

    static defaultProps = {
        menu: [],
        navigation: {
            navigate: () => {},
        },
    }

    static _titleFormater(text) {
        const arrText = text.split(' ');

        if (arrText.length > 1) {
            return `${arrText[0]} ${arrText[1].toUpperCase()}`;
        }

        return text;
    }

    componentWillMount() {
        const { key } = this.props.navigation.state;

        this.props.actions.setHomeKey(key);
    }

    render() {
        const { navigation, menu } = this.props;
        // const isMenuValid = menu.length > 0 && Object.prototype.hasOwnProperty.call(menu[0], 'petak');
        // console.log(menu);

        return (
            <View style={styles.container}>
                <View style={styles.headingTextContainer}>
                    <Text style={styles.headingText}>
                        Lakukan pengisian data petak dan mangrove anda.
                    </Text>
                </View>
                {/* <ButtonWhite
                    text={isMenuValid ? this.constructor._titleFormater(menu[0].petak[0].name) : 'Petak 8A'}
                    pressEvent={() => {
                        navigation.navigate('ClusterMenu', (isMenuValid ? menu[0].petak[0] : { name: 'Petak 8A' }));
                    }}
                />
                <ButtonWhite
                    text={isMenuValid ? this.constructor._titleFormater(menu[0].petak[1].name) : 'Petak 8B'}
                    pressEvent={() => {
                        navigation.navigate('ClusterMenu', (isMenuValid ? menu[0].petak[1] : { name: 'Petak 8B' }));
                    }}
                />
                <ButtonWhite
                    text={isMenuValid ? this.constructor._titleFormater(menu[0].petak[2].name) : 'Mangrove 8'}
                    pressEvent={() => {
                        navigation.navigate('MangroveMenu', (isMenuValid ? menu[0].petak[2] : { name: 'Mangrove 8' }));
                    }}
                /> */}
                {
                    menu.length > 0 &&
                    menu.map((cluster) => {
                        if ('petak' in cluster && cluster.petak.length > 0) {
                            return cluster.petak.map(petak => (
                                <ButtonWhite
                                    key={petak.petak_id}
                                    text={this.constructor._titleFormater(petak.name || 'Petak')}
                                    pressEvent={() => {
                                        navigation.navigate((petak.type.toLowerCase() === 'swath' ? 'ClusterMenu' : 'MangroveMenu'), (Object.keys(petak).length > 0 ? petak : { name: (petak.type.toLowerCase() === 'swath' ? 'Petak' : 'Mangrove') }));
                                    }}
                                />
                            ));
                        }

                        return null;
                    })
                }
            </View>
        );
    }
}
