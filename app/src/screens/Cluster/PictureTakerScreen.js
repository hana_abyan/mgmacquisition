import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, Image, TouchableNativeFeedback } from 'react-native';
import Camera, { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/FontAwesome';

import ContainerHOC from '../../core/ContainerHOC';

import * as clustersActionCreators from '../../data/clusters/actions';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    captureHolder: {
        flex: 0,
        position: 'absolute',
        bottom: 30,
        backgroundColor: '#fff',
        width: 64,
        height: 64,
        borderRadius: 64 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 5,
        borderColor: '#fff',
    },
    capture: {
        flex: 0,
        // backgroundColor: '#fff',
        // borderRadius: 5,
        // color: '#000',
        // padding: 10,
        // margin: 40,
        // paddingLeft: 3,
        // paddingTop: 3,
        // position: 'absolute',
        // bottom: 30,
        // padding: 0,
        // width: 50,
        // height: 56,
        // borderRadius: 56 / 2,
        // borderWidth: 5,
        // borderColor: '#fff',
    },
    spinner: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imagePreview: {
        flexDirection: 'column',
        marginTop: 20,
    },
    imageWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 3,
    },
    imageHolder: {
        flexDirection: 'row',
        height: 300,
        overflow: 'hidden',
    },
    image: {
        alignSelf: 'stretch',
    },
    actionWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 50,
        paddingLeft: 30,
        paddingRight: 30,
        paddingBottom: 50,
        alignItems: 'flex-end',
    },
    btnSave: {
        flex: 1,
        justifyContent: 'flex-end',
        textAlign: 'right',
        fontSize: 18,
        fontFamily: 'MyriadProBold',
        fontWeight: '500',
        color: '#2E7BB3',
    },
});

@connect(
    state => ({
        state,
        images: state.clusters.image,
    }),
    dispatch => ({
        setUploadStatus: (isPosting, uploadStatus, payload) => dispatch(clustersActionCreators.setUploadImageStatus(isPosting, uploadStatus, payload)),
    }),
)
@ContainerHOC()
export default class PictureTakerScreen extends Component {
    state = {
        type: 'camera',
        imagePath: '',
    }

    async _takePicture() {
        if (this.camera) {
            const thisCam = this.camera;

            this.setState({ type: 'loading' });
            const options = {
                quality: 0.7,
                base64: false,
                width: 1152, // realisation set height of output
                fixOrientation: true,
            };
            const data = await thisCam.takePictureAsync(options);
            this.setState({ type: 'preview', imagePath: data.uri });
            // this.camera.capture({ metadata: options })
            //     .then((data) => {
            //         console.log(data);
            //         this.setState({ type: 'preview', imageUri: data.mediaUri, imagePath: data.path }, () => console.log(this.state));
            //     })
            //     .catch(err => console.error(err));
        }
    }

    _saveHandler() {
        const { navigation } = this.props;
        navigation.goBack(null);
        navigation.state.params.setImagePath(this.state.imagePath);
    }

    render() {
        const { type, imagePath } = this.state,
            { windowWidth } = this.props;

        return (
            <View style={styles.container}>
                {
                    (type === 'camera' || type === 'loading') &&
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <RNCamera
                            ref={(cam) => {
                                this.camera = cam;
                            }}
                            // onBarCodeRead={this.onBarCodeRead.bind(this)}
                            style={styles.preview}
                            aspect={Camera.constants.Aspect.fill}
                            type={RNCamera.Constants.Type.back}
                            permissionDialogTitle="Permission to use camera"
                            permissionDialogMessage="We need your permission to use your camera phone"
                        />
                            {
                                type === 'camera' &&
                                // <Text style={styles.capture} onPress={() => this._takePicture()}>[CAPTURE]</Text>
                                <View style={styles.captureHolder}>
                                <Icon
                                    name="circle-o"
                                    size={60}
                                    backgroundColor="#fffff"
                                    color="#072B70"
                                    style={styles.capture}
                                    onPress={() => this._takePicture()}
                                />
                                </View>
                            }
                            {
                                type === 'loading' &&
                                <View style={{
                                        backgroundColor: '#fff',
                                        position: 'absolute',
                                        top: 0,
                                        right: 0,
                                        bottom: 0,
                                        left: 0,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    {/* <ActivityIndicator size="large" color="#1A537C" /> */}
                                    <Text>Mohon tunggu...</Text>
                                </View>
                            }
                    </View>
                }

                {
                    type === 'preview' &&
                    <View style={styles.imagePreview}>
                        <View style={styles.imageWrapper}>
                            <View style={styles.imageHolder}>
                                <Image
                                    resizeMode={Image.resizeMode.cover}
                                    style={[styles.image, { width: windowWidth }]}
                                    source={{ isStatic: true, uri: imagePath }}
                                />
                            </View>
                        </View>

                        <View style={styles.actionWrapper}>
                            <TouchableNativeFeedback
                                background={TouchableNativeFeedback.SelectableBackground()}
                                onPress={() => this._saveHandler()}
                            >
                                <Text style={styles.btnSave}>SIMPAN</Text>
                            </TouchableNativeFeedback>
                        </View>
                    </View>
                }
            </View>
        );
    }
}
