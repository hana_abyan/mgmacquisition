import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ScrollView, View, Text, StyleSheet, TouchableNativeFeedback, DatePickerAndroid, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';
import moment from 'moment';

import ContainerHOC from '../../core/ContainerHOC';
import InputText from '../../components/InputText';
import ButtonBlue from '../../components/ButtonBlue';
import Message from '../../components/Message';

import * as helper from '../../helper';

import * as clustersActionCreators from '../../data/clusters/actions';
import * as clusters from '../../data/clusters';

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 20,
        paddingBottom: 40,
        paddingLeft: 15,
        paddingRight: 15,
    },
    formContainer: {
        marginTop: 20,
    },
    formInner: {
        backgroundColor: '#fff',
        padding: 20,
    },
    formGroup: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    formLabel: {
        flex: 2,
    },
    labelText: {
        color: '#1A5185',
        fontFamily: 'MyriadProBold',
    },
    formControl: {
        flex: 3,
        paddingLeft: 15,
    },
    formInfo: {
        flex: 1,
    },
    infoText: {
        flex: 0,
        color: '#1A5185',
        paddingLeft: 10,
        fontFamily: 'MyriadProBold',
    },
    buttonWrapper: {
        marginTop: 10,
    },
    messageContainer: {
        marginTop: 10,
    },
});

@connect(
    state => ({
        state,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { center: 'Input Data Ketinggian' } })
export default class ShrimpInputScreen extends Component {
    initialState = {
        tmangrove: '',
        dateTaken: '',
        status: false,
        message: '',
        isLoading: false,
    }

    state = {
        ...this.initialState,
    }

    _submitHandler() {
        const { dateTaken, tmangrove } = this.state,
            { navigation: { state } } = this.props;

        this.setState({ isLoading: true });

        if (tmangrove && tmangrove.length > 0 && dateTaken && dateTaken.length > 0) {
            const payload = state.params.data.map(item => ({
                ...item, chart_id: item.id, value: this.state[item.type] === '' ? 0 : this.state[item.type], data_taken: moment(dateTaken, 'YYYY/MM/DD').format('YYYY-MM-DD'),
            }));

            clusters.createChart(payload)
                .then((resolve) => {
                    if (resolve.status.toLowerCase() === 'ok') {
                        this.setState({ status: true, message: 'Ubah Data Berhasil!', isLoading: false }, () => this.setState({ tmangrove: '', dateTaken: '' }));
                        this.props.actions.getChart({ petak_id: 'params' in state ? state.params.petakId : null });
                    } else {
                        this.setState({ isLoading: false });
                    }
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e || 'data' in e || 'error' in e) {
                        error = e.message || e.data || e.error;
                    }

                    const message = typeof error === 'object' ? Object.values(error).reduce((prev, current) => [...prev, ...current], []) : error;

                    this.setState({ status: false, message, isLoading: false });
                });
        } else {
            this.setState({ status: false, message: 'Periksa kembali data anda. Tanggal dan tinggi mangrove harus diisi.', isLoading: false });
        }
    }

    _changeDate(value) {
        if (value > moment().format('YYYY/MM/DD')) {
            alert('Periksa tanggal. Pilih tanggal maksimal per hari ini.');
        } else {
            this.setState({ dateTaken: value });
        }
    }

    render() {
        const {
            tmangrove, dateTaken, status, message, isLoading,
        } = this.state;

        return (
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag"
            >
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={styles.container}>
                        <View style={styles.formContainer}>
                            <TouchableNativeFeedback
                                onPress={async () => {
                                    try {
                                        const {
                                            action, year, month, day,
                                        } = await DatePickerAndroid.open({
                                                date: dateTaken === '' ? new Date() : new Date(dateTaken),
                                            });

                                        if (action !== DatePickerAndroid.dismissedAction) {
                                            // this.setState({ dateTaken: `${year}/${month + 1}/${day}` });
                                            this._changeDate(`${year}/${helper.zeroAdd(month + 1)}/${helper.zeroAdd(day)}`);
                                        }
                                    } catch ({ code, message }) {
                                        console.warn('Cannot open date picker', message);
                                    }
                                }}
                            >
                                <View
                                    style={{
                                        backgroundColor: '#fff',
                                        padding: 20,
                                        marginBottom: 10,
                                    }}
                                >
                                    <Text>
                                        {dateTaken !== '' ? dateTaken : 'Pilih Tanggal'}
                                    </Text>
                                </View>
                            </TouchableNativeFeedback>

                            <View style={styles.formInner}>
                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Tinggi Mangrove</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={tmangrove}
                                            changeEvent={val => this.setState({ tmangrove: val })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'done',
                                                blurOnSubmit: true,
                                                keyboardType: 'numeric',
                                            }}
                                        />
                                    </View>
                                    <View style={styles.formInfo}>
                                        <Text style={styles.infoText}>cm</Text>
                                    </View>
                                </View>
                            </View>

                            {
                                typeof message === 'string' && message !== '' &&
                                <View style={styles.messageContainer}>
                                    <Message
                                        type={status ? 'success' : 'error'}
                                        message={message}
                                    />
                                </View>
                            }

                            {
                                typeof message === 'object' && message.length > 0 &&
                                <View style={styles.messageContainer}>
                                    {
                                        message.map(item => (
                                            <Message
                                                key={item}
                                                type={status ? 'success' : 'error'}
                                                message={item}
                                            />
                                        ))
                                    }
                                </View>
                            }

                            <View style={styles.buttonWrapper}>
                                <ButtonBlue
                                    text="Simpan"
                                    pressEvent={() => {
                                        Alert.alert('Konfirmasi', 'Kirim data ketinggian mangrove?', [
                                            { text: 'Cancel', onPress: () => {}, style: 'cancel' },
                                            { text: 'OK', onPress: () => this._submitHandler() },
                                        ]);
                                    }}
                                    isLoading={isLoading}
                                />
                            </View>
                        </View>

                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
        );
    }
}
