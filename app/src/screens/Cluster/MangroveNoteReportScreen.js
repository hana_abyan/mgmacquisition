import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, View, StyleSheet, BackHandler } from 'react-native';
import { ActionButton } from 'react-native-material-ui';

import ContainerHOC from '../../core/ContainerHOC';
import MangroveNoteRow from '../../components/clusters/MangroveNoteRow';

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 20,
        paddingBottom: 40,
        paddingLeft: 15,
        paddingRight: 15,
    },
    picker: {
        backgroundColor: 'white',
    },
    chartSection: {
        marginTop: 15,
    },
    table: {
        backgroundColor: '#fff',
        marginTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 20,
    },
});

@connect(state => ({
    notes: state.clusters.note,
}))
@ContainerHOC({ header: { center: 'Input Data Keterangan' } })
export default class ShrimpReportScreen extends Component {
    state = {
        data: [],
    }

    componentWillMount() {
        const { state } = this.props.navigation;

        BackHandler.addEventListener('hardwareBackPress', () => {
            const { state } = this.props.navigation,
            { backToDrawer } = this.props;

            if ('params' in state && state.params && 'routeFrom' in state.params) {
                backToDrawer();
            }
        });

        if ('params' in state) {
            this._setData(state.params.data.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.notes !== nextProps.notes) {
            this._setData(nextProps.notes);
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    _setData(data) {
        this.setState({ data });
    }

    render() {
        const { data } = this.state,
            { navigation, navigation: { state } } = this.props;

        const payload = 'params' in state && 'data' in state.params ? { noteId: state.params.data.keterangan_id, petakId: state.params.petakId } : {};

        return (
            <View style={{ paddingBottom: 150 }}>
                <ScrollView style={styles.scroll}>
                    <View style={styles.container}>
                        <View style={styles.table}>
                            <MangroveNoteRow
                                no="No"
                                keterangan="Keterangan"
                                jumlah="Jumlah"
                                header
                            />
                            {data.length > 0 &&
                                data.map((item, index) => (
                                    <MangroveNoteRow
                                        key={`mangrove-note-${index}`}
                                        no={index + 1}
                                        keterangan={item.label}
                                        jumlah={item.value}
                                    />
                                ))
                            }
                        </View>
                    </View>
                </ScrollView>
                <ActionButton
                    icon="create"
                    style={{
                        container: {
                            backgroundColor: '#072B70', position: 'absolute', bottom: 50, right: 10,
                        },
                    }}
                    onPress={() => navigation.navigate('MangroveNoteInput', payload)}
                />
            </View>
        );
    }
}
