import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { StyleSheet, View, ScrollView, Image, Text, TouchableOpacity, BackHandler } from 'react-native';
import { ActionButton } from 'react-native-material-ui';
import * as Progress from 'react-native-progress';
import RNFetchBlob from 'react-native-fetch-blob';

import ContainerHOC from '../../core/ContainerHOC';
import * as clustersActionCreators from '../../data/clusters/actions';
import * as clusters from '../../data/clusters/index';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        paddingBottom: 50,
    },
    gallery: { // scroll
        marginTop: 5,
        flexDirection: 'column',
    },
    tabs: {
        flexDirection: 'row',
        backgroundColor: '#333',
        padding: 20,
    },
    tab: {
        flex: 1,
    },
    icon: {
        textAlign: 'center',
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        margin: 5,
    },
    imageHolder: {
        flex: 1,
    },
    photo: {
        height: 100,
        overflow: 'hidden',
        marginLeft: 5,
        marginRight: 5,
        flex: 1,
    },
    progressContainer: {
        alignSelf: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    progressText: {
        alignSelf: 'center',
        marginTop: 5,
    },
});

@connect(
    state => ({
        postingStatus: state.clusters.isPosting,
        postingStatusResult: state.clusters.uploadStatus,
        postingResult: state.clusters.uploadResult,
        images: state.clusters.image,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    })
)
@ContainerHOC()
export default class PictureGalleryScreen extends Component {
    static getPairsArray = (photos) => {
        const pairsR = [];

        let pairs = [],
            count = 0;

        photos.forEach((item) => {
            count += 1;
            pairs.push(item);
            if (count === 2) {
                pairsR.push(pairs);
                count = 0;
                pairs = [];
            }
        });

        if (count === 1) {
            pairs.push({
                label: 'photonone',
                panen_image: '',
                panen_image_id: 0,
            });

            pairsR.push(pairs);
        }

        return pairsR;
    }

    constructor(props) {
        super(props);
        // this.props.navigation.state.key = 'PG';

        this.state = {
            photos: [],
            progress: 0,
            takenImagePath: '',
        };
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            const { state } = this.props.navigation,
            { backToDrawer } = this.props;

            if ('params' in state && state.params && 'routeFrom' in state.params) {
                backToDrawer();
            }
        });

        if ('params' in this.props.navigation.state) {
            this._setData(this.props.navigation.state.params.data);
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    _setData(data) {
        this.setState({ photos: data });
    }

    _uploadImage(imagePath) {
        const { navigation: { state } } = this.props,
            // petakId = 'params' in state && state.params && 'petak_id' in state.params ? state.params.petak_id : null,
            petakId = 'params' in state && state.params && 'petakId' in state.params ? state.params.petakId : null,
            data = {
                filename: 'image.jpg',
                type: 'image/jpg',
                data: RNFetchBlob.wrap(imagePath),
            };

        clusters.postImage([{ name: 'panen_id', data: `${petakId}` }, { name: 'images[0]', ...data }])
            .then((resolve) => {
                const resData = typeof resolve.data === 'string' ? JSON.parse(resolve.data) : resolve.data;

                if ('status' in resData && resData.status.toLowerCase() === 'ok') {
                    this._uploadSuccess(resData.data[0]);
                }

                if (Object.prototype.hasOwnProperty.call(resData, 'error')) {
                    const arrError = Object.values(resData.error);

                    if (typeof arrError === 'object' && arrError.length > 0) {
                        let error = '';

                        arrError.map((item) => { error += item; });
                        this._uploadFailed();
                        alert(error);
                    }
                }
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                this._uploadFailed();
                alert(error);
            });
    }

    _uploadSuccess(result) {
        const photos = JSON.parse(JSON.stringify(this.state.photos)),
        idxNew = photos.findIndex(item => item.panen_image === 'new');

        photos[idxNew].panen_image = result.image;
        photos[idxNew].panen_image_id = result.id;

        this.setState({ progress: 1 }, () => setTimeout(() => this.setState({ photos }), 200));
    }

    _uploadFailed() {
        const photos = JSON.parse(JSON.stringify(this.state.photos)),
        idxNew = photos.findIndex(item => item.panen_image === 'new');

        photos.splice(idxNew, 1);

        this.setState({ progress: 1, photos });
    }

    _deleteHandler(idImage) {
        const photos = JSON.parse(JSON.stringify(this.state.photos)),
        idxDeleted = photos.findIndex(item => item.panen_image_id === idImage);

        photos.splice(idxDeleted, 1);

        this.setState({ photos });
    }

    _setTakenImagePath(takenImagePath) {
        const photos = JSON.parse(JSON.stringify(this.state.photos));
        photos.push({ panen_image: 'new', panen_image_id: 'new-image' });

        this.setState({ takenImagePath, photos }, () => {
            this._restart();
            this._uploadImage(this.state.takenImagePath);
        });
    }

    _increase() {
        const progress = this.state.progress + 0.1;
        if (progress > 1) {
            clearTimeout(this.tm);
            return;
        }
        this.setState({ progress });
        this.tm = setTimeout(() => this._increase(), 100);
    }

    _restart() {
        clearTimeout(this.tm);
        this.setState({ progress: 0 }, () => {
            this._increase();
        });
    }

    renderGallery() {
        const pairs = this.constructor.getPairsArray(this.state.photos),
            { progress } = this.state,
            { navigation, navigation: { state } } = this.props;

        // const petakId = 'params' in state && state.params && 'petak_id' in state.params ? state.params.petak_id : null;
        const petakId = 'params' in state && state.params && 'petakId' in state.params ? state.params.petakId : null;

        if (pairs.length > 0) {
            return pairs.map(item => (
                <View style={styles.item} key={item[0].panen_image_id}>
                    {
                        item[0].panen_image === 'new' &&
                        <View
                            style={styles.photo}
                        >
                            <View style={styles.progressContainer}>
                                <Progress.Bar
                                    progress={progress}
                                    borderRadius={0}
                                    color="#1A537C"
                                    unfilledColor="#DDDDDD"
                                    height={12}
                                    borderWidth={0}
                                />
                                <Text style={styles.progressText}>Uploading...</Text>
                            </View>
                        </View>
                    }

                    {
                        item[0].panen_image !== 'new' &&
                        <TouchableOpacity
                            onPress={() => navigation.navigate('PictureDetail', {
                                imageSource: item[0].panen_image,
                                imageId: item[0].panen_image_id,
                                petakId,
                                deleteEvent: () => this._deleteHandler(item[0].panen_image_id),
                            })}
                            style={styles.imageHolder}
                        >
                            <Image
                                resizeMode={Image.resizeMode.cover}
                                style={styles.photo}
                                source={{ uri: item[0].panen_image }}
                            />
                        </TouchableOpacity>
                    }

                    {
                        item[1].label !== 'photonone' && item[1].panen_image !== 'new' &&
                        <TouchableOpacity
                            onPress={() => navigation.navigate('PictureDetail', {
                                imageSource: item[1].panen_image,
                                imageId: item[1].panen_image_id,
                                petakId,
                                deleteEvent: () => this._deleteHandler(item[1].panen_image_id),
                            })}
                            style={styles.imageHolder}
                        >
                            <Image
                                resizeMode={Image.resizeMode.cover}
                                style={styles.photo}
                                source={{ uri: item[1].panen_image }}
                            />
                        </TouchableOpacity>
                    }

                    {
                        item[1].label !== 'photonone' && item[1].panen_image === 'new' &&
                        <View
                            style={styles.imageHolder}
                        >
                            <View style={styles.progressContainer}>
                                <Progress.Bar
                                    progress={progress}
                                    borderRadius={0}
                                    color="#1A537C"
                                    unfilledColor="#DDDDDD"
                                    height={12}
                                    borderWidth={0}
                                />
                                <Text style={styles.progressText}>Uploading...</Text>
                            </View>
                        </View>
                    }

                    {
                        item[1].label === 'photonone' && (
                            <View
                                style={styles.imageHolder}
                            />
                        )
                    }
                </View>
            ));
        }

        return <View />;
    }

    // _afterDeleteImage() {
    //     /* delete dari array */
    // }

    render() {
        const { navigation } = this.props;

        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={styles.gallery}>
                    <View style={styles.container}>
                        {this.renderGallery()}
                    </View>
                </ScrollView>
                <ActionButton
                    icon="camera-alt"
                    style={{
                        container: {
                            backgroundColor: '#072B70', position: 'absolute', bottom: 0, right: 5,
                        },
                    }}
                    onPress={() => navigation.navigate('PictureTaker', {
                        setImagePath: imagePath => this._setTakenImagePath(imagePath),
                    })}
                />
            </View>
        );
    }
}
