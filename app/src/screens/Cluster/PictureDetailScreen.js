import React, { Component } from 'react';
import { View, Image, StyleSheet, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ContainerHOC from '../../core/ContainerHOC';

import * as clusters from '../../data/clusters';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    image: {
        flex: 1,
    },
    actionContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        paddingRight: 30,
    },
});

@ContainerHOC()
export default class PictureDetailScreen extends Component {
    _deleteHandler() {
        const { navigation, navigation: { state } } = this.props,
            data = {
                panen_id: `${state.params.petakId}`,
                'panen_image_id[0]': `${state.params.imageId}`,
            };

        const payload = Object.keys(data).map(key => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`).join('&');

        clusters.deleteImage(payload)
            .then((resolve) => {
                if (resolve.ok && resolve.status === 200) {
                    state.params.deleteEvent();
                }
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e || 'data' in e) {
                    error = e.message || e.data;
                }

                alert(error);
            });

        navigation.goBack(null);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image
                        resizeMode={Image.resizeMode.cover}
                        source={{ uri: this.props.navigation.state.params.imageSource }}
                        style={styles.image}
                    />
                </View>
                <View style={styles.actionContainer}>
                    <Icon
                        name="trash"
                        size={30}
                        color="#676767"
                        onPress={() => {
                            Alert.alert('Konfirmasi', 'Hapus foto?', [
                                { text: 'Cancel', onPress: () => {}, style: 'cancel' },
                                { text: 'OK', onPress: () => this._deleteHandler() },
                            ]);
                        }}
                    />
                </View>
            </View>
        );
    }
}
