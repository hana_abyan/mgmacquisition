import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';

import ButtonWhite from '../../components/ButtonWhite';
import ContainerHOC from '../../core/ContainerHOC';

import * as clustersActionCreators from '../../data/clusters/actions';

const IconProgress = require('../../assets/images/icon_progress.png');
const IconWater = require('../../assets/images/icon_kualitas_air.png');
const IconShrimp = require('../../assets/images/icon_data_udang.png');
const IconPhoto = require('../../assets/images/icon_foto.png');

const styles = StyleSheet.create({
    container: {
        paddingLeft: 25,
        paddingRight: 25,
    },
});

@connect(
    state => ({
        state,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
@ContainerHOC()
export default class MenuScreen extends Component {
    static propTypes = {
        navigation: PropTypes.shape({
            navigate: PropTypes.func.isRequired,
        }).isRequired,
    };

    _navigationHandler(route) {
        const { navigation, navigation: { state } } = this.props;

        if (route === 'UpdateProgress') {
            this.props.actions.getTask({ petakId: ('params' in state ? state.params.petak_id : null) })
                .then((resolve) => {
                    navigation.navigate(route, { data: resolve, petakId: state.params.petak_id });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        } else if (route === 'WaterQualityReport' || route === 'ShrimpReport') {
            this.props.actions.getChart({ petak_id: ('params' in state ? state.params.petak_id : null) })
                .then((resolve) => {
                    navigation.navigate(route, { data: resolve, petakId: 'params' in state ? state.params.petak_id : null });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        } else if (route === 'PictureGallery') {
            this.props.actions.getImage({ petakId: ('params' in state ? state.params.petak_id : null) })
                .then((resolve) => {
                    navigation.navigate(route, { data: resolve, ...('params' in state ? state.params : {}), petakId: ('params' in state && state.params ? state.params.petak_id : null) });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        }
    }

    render() {
        const { navigation, navigation: { state: routeState } } = this.props;
        return (
            <View style={styles.container}>
                <ButtonWhite
                    icon={IconProgress}
                    text="Progress Update"
                    pressEvent={() => this._navigationHandler('UpdateProgress')}
                    textAlign="left"
                />

                <ButtonWhite
                    icon={IconWater}
                    text="Input Kualitas Air"
                    pressEvent={() => this._navigationHandler('WaterQualityReport')}
                    textAlign="left"
                />

                <ButtonWhite
                    icon={IconShrimp}
                    text="Input Data Udang"
                    pressEvent={() => this._navigationHandler('ShrimpReport')}
                    textAlign="left"
                />

                <ButtonWhite
                    icon={IconPhoto}
                    text="Foto Petak"
                    pressEvent={() => this._navigationHandler('PictureGallery')}
                    textAlign="left"
                />
            </View>
        );
    }
}
