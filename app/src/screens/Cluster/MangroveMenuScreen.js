import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';

import ButtonWhite from '../../components/ButtonWhite';
import ContainerHOC from '../../core/ContainerHOC';

import * as clustersActionCreators from '../../data/clusters/actions';

const IconProgress = require('../../assets/images/icon_progress.png');
const IconMangrove = require('../../assets/images/icon_mangrove.png');
const IconInfo = require('../../assets/images/icon_info.png');
const IconPhoto = require('../../assets/images/icon_foto.png');

const styles = StyleSheet.create({
    container: {
        paddingLeft: 25,
        paddingRight: 25,
    },
});

@connect(
    state => ({
        state,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
@ContainerHOC()
export default class MenuScreen extends Component {
    static propTypes = {
        navigation: PropTypes.shape({
            navigate: PropTypes.func.isRequired,
        }).isRequired,
    };

    _navigationHandler(route) {
        const { navigation, navigation: { state } } = this.props;

        if (route === 'UpdateProgress') {
            this.props.actions.getTask({ petakId: ('params' in state ? state.params.petak_id : null) })
                .then((resolve) => {
                    navigation.navigate(route, { data: resolve, petakId: state.params.petak_id });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        } else if (route === 'MangroveGrowth') {
            this.props.actions.getChart({ petak_id: ('params' in state ? state.params.petak_id : null) })
                .then((resolve) => {
                    navigation.navigate(route, { data: resolve });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        } else if (route === 'MangroveNote') {
            this.props.actions.getNote({ petakId: ('params' in state ? state.params.petak_id : null) })
                .then((resolve) => {
                    navigation.navigate(route, { data: resolve, petakId: state.params.petak_id });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        } else if (route === 'PictureGallery') {
            this.props.actions.getImage({ petakId: ('params' in state ? state.params.petak_id : null) })
                .then((resolve) => {
                    navigation.navigate(route, { data: resolve, ...('params' in state ? state.params : {}), petakId: ('params' in state && state.params ? state.params.petak_id : null) });
                    // (Object.prototype.hasOwnProperty.call(routeState, 'params') ? navigation.navigate('PictureGallery', routeState.params) : navigation.navigate('PictureGallery'))
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        }
    }

    render() {
        const { navigation, navigation: { state: routeState } } = this.props;
        return (
            <View style={styles.container}>
                <ButtonWhite
                    icon={IconProgress}
                    text="Progress Update"
                    pressEvent={() => this._navigationHandler('UpdateProgress')}
                    textAlign="left"
                />

                <ButtonWhite
                    icon={IconMangrove}
                    text="Input Data Ketinggian"
                    pressEvent={() => this._navigationHandler('MangroveGrowth')}
                    textAlign="left"
                />

                <ButtonWhite
                    icon={IconInfo}
                    text="Input Data Keterangan"
                    pressEvent={() => this._navigationHandler('MangroveNote')}
                    textAlign="left"
                />

                <ButtonWhite
                    icon={IconPhoto}
                    text="Foto Petak"
                    pressEvent={() => this._navigationHandler('PictureGallery')}
                    textAlign="left"
                />
            </View>
        );
    }
}
