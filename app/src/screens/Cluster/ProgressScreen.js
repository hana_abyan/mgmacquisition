import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, StyleSheet, ScrollView, Alert, BackHandler } from 'react-native';

import ProgressList from '../../components/progress/List';
import ContainerHOC from '../../core/ContainerHOC';

import * as clustersActionCreators from '../../data/clusters/actions';
import * as clusters from '../../data/clusters';

const styles = StyleSheet.create({
    container: {
        paddingLeft: 25,
        paddingRight: 25,
    },
});

@connect(
    state => ({
        tasks: state.clusters.task,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { center: 'Progress Update' } })
export default class ProgressScreen extends Component {
    state = {
        type: 'petak',
        taskList: [],
        isOpened: [false, false, false],
    };

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            const { state } = this.props.navigation,
            { backToDrawer } = this.props;

            if ('params' in state && state.params && 'routeFrom' in state.params) {
                backToDrawer();
            }
        });

        if ('params' in this.props.navigation.state) {
            this._setTaskList(this.props.navigation.state.params.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.tasks !== nextProps.tasks) {
            this._setTaskList(nextProps.tasks);
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    _setTaskList(data) {
        this.setState({ taskList: data });
    }

    _doOpen(index) {
        const a = [...this.state.isOpened];
        a[index] = !this.state.isOpened[index];
        this.setState({
            isOpened: a,
        });
    }

    _updateTaskHandler(taskDetailId, index) {
        const { navigation: { state } } = this.props,
        taskList = JSON.parse(JSON.stringify(this.state.taskList));
        const idxDetail = taskList[index].taskdetails.findIndex(item => item.taskdetail_id === taskDetailId);

        if (idxDetail > -1 && taskList[index].taskdetails[idxDetail].progress === 0) {
            taskList[index].taskdetails[idxDetail].progress = 100;
            this.setState({ taskList });
        }

        clusters.editTask({ task_detail: taskDetailId })
            .then((resolve) => {
                if (resolve.status.toLowerCase() === 'ok') {
                    this.props.actions.getTask({ petakId: ('params' in state ? state.params.petakId : null) });
                }
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                taskList[index].taskdetails[idxDetail].progress = 0;
                this.setState({ taskList });

                alert(error);
            });
    }

    render() {
        const { type, taskList } = this.state;

        return (
            <ScrollView>
                <View stlye={styles.container}>
                    {taskList && taskList.length > 0 &&
                        taskList.map((item, index) => (
                            <ProgressList
                                key={item.type.toLowerCase() === 'swath' ? `group-list-${item.task_id}` : `group-list-${item.task_id}`}
                                number={index}
                                name={item.name}
                                data={item.taskdetails}
                                isOpen={type === 'petak' ? this.state.isOpened[index] : true}
                                openHandler={type === 'petak' ? val => this._doOpen(val) : () => { }}
                                progressValue={item.progress || 0}
                                isDone={item.done || false}
                                dueDate={item.due_date_view || ''}
                                changeEvent={(taskDetailId, idx) => {
                                    Alert.alert('Konfirmasi', 'Ubah data progress?', [
                                        { text: 'Cancel', onPress: () => {}, style: 'cancel' },
                                        { text: 'OK', onPress: () => this._updateTaskHandler(taskDetailId, idx) },
                                    ]);
                                }}
                            />
                        ))
                    }
                </View>
            </ScrollView>
        );
    }
}
