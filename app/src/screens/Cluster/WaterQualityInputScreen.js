import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ScrollView, View, Text, StyleSheet, TouchableNativeFeedback, DatePickerAndroid, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';
import moment from 'moment';

import ContainerHOC from '../../core/ContainerHOC';
import ButtonBlue from '../../components/ButtonBlue';
import InputText from '../../components/InputText';
import Message from '../../components/Message';

import * as helper from '../../helper';

import * as clustersActionCreators from '../../data/clusters/actions';
import * as clusters from '../../data/clusters';

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 20,
        paddingBottom: 40,
        paddingLeft: 15,
        paddingRight: 15,
    },
    formContainer: {
        marginTop: 20,
    },
    formInner: {
        backgroundColor: '#fff',
        padding: 20,
    },
    formGroup: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    formLabel: {
        flex: 2,
    },
    labelText: {
        color: '#1A5185',
    },
    formControl: {
        flex: 3,
        paddingLeft: 15,
    },
    formInfo: {
        flex: 1,
    },
    infoText: {
        flex: 0,
        color: '#1A5185',
        paddingLeft: 10,
    },
    buttonWrapper: {
        marginTop: 10,
    },
    messageContainer: {
        marginTop: 10,
    },
});

@connect(
    state => ({
        state,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { center: 'Input Kualitas Air' } })
export default class WaterQualityInputScreen extends Component {
    initialState = {
        ph: '',
        salinitas: '',
        oksigen: '',
        dateTaken: '',
        status: false,
        message: '',
        isLoading: false,
    }

    state = {
        ...this.initialState,
    }

    inputs = {}

    _submitHandler() {
        const {
            dateTaken, ph, salinitas, oksigen,
        } = this.state,
            { navigation: { state } } = this.props;

        this.setState({ isLoading: true });
        if (dateTaken && dateTaken.length > 0 && ((ph && ph.length > 0) || (salinitas && salinitas.length > 0) || (oksigen && oksigen.length > 0))) {
            const payload = state.params.data.map(item => ({
                ...item, chart_id: item.id, value: this.state[item.type] === '' ? 0 : this.state[item.type], data_taken: moment(dateTaken, 'YYYY/MM/DD').format('YYYY-MM-DD'),
            }));

            clusters.createChart(payload)
                .then((resolve) => {
                    if (resolve.status.toLowerCase() === 'ok') {
                        this.setState({ status: true, message: 'Ubah Data Berhasil!', isLoading: false }, () => this.setState({
                            ph: '', salinitas: '', oksigen: '', dateTaken: '',
                        }));
                        this.props.actions.getChart({ petak_id: 'params' in state ? state.params.petakId : null });
                    } else {
                        this.setState({ isLoading: false });
                    }
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e || 'data' in e || 'error' in e) {
                        error = e.message || e.data || e.error;
                    }

                    const message = typeof error === 'object' ? Object.values(error).reduce((prev, current) => [...prev, ...current], []) : error;

                    this.setState({ message, status: false, isLoading: false });
                });
        } else {
            this.setState({ status: false, message: 'Periksa kembali data anda. Tanggal dan salah satu variabel masukan harus diisi.', isLoading: false });
        }
    }

    _changeDate(value) {
        if (value > moment().format('YYYY/MM/DD')) {
            alert('Periksa tanggal. Pilih tanggal maksimal per hari ini.');
        } else {
            this.setState({ dateTaken: value });
        }
    }

    _setRef(key, com) {
        this.inputs[key] = com;
    }

    _focusNextField(id) {
        this.inputs[id].focus();
    }

    render() {
        const {
            ph, salinitas, oksigen, dateTaken, status, message, isLoading,
        } = this.state;

        return (
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag"
            >
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={styles.container}>
                        <View style={styles.formContainer}>
                            <TouchableNativeFeedback
                                onPress={async () => {
                                    try {
                                        const {
                                            action, year, month, day,
                                        } = await DatePickerAndroid.open({
                                            date: dateTaken === '' ? new Date() : new Date(dateTaken),
                                        });

                                        if (action !== DatePickerAndroid.dismissedAction) {
                                            // this.setState({ dateTaken: `${year}/${month + 1}/${day}` });
                                            this._changeDate(`${year}/${helper.zeroAdd(month + 1)}/${helper.zeroAdd(day)}`);
                                        }
                                    } catch ({ code, msg }) {
                                        console.warn('Cannot open date picker', msg);
                                    }
                                }}
                            >
                                <View
                                    style={{
                                        backgroundColor: '#fff',
                                        padding: 20,
                                        marginBottom: 10,
                                    }}
                                >
                                    <Text>
                                        {dateTaken !== '' ? dateTaken : 'Pilih Tanggal'}
                                    </Text>
                                </View>
                            </TouchableNativeFeedback>

                            <View style={styles.formInner}>
                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Ph Balance</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={ph}
                                            changeEvent={val => this.setState({ ph: val })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'next',
                                                blurOnSubmit: false,
                                                keyboardType: 'numeric',
                                            }}
                                            setRef={c => this._setRef('one', c)}
                                            submitEditingHandler={() => this._focusNextField('two')}
                                        />
                                    </View>
                                    <View style={styles.formInfo}>
                                        <Text style={styles.infoText} />
                                    </View>
                                </View>

                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Kadar Salinitas</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={salinitas}
                                            changeEvent={val => this.setState({ salinitas: val })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'next',
                                                blurOnSubmit: false,
                                                keyboardType: 'numeric',
                                            }}
                                            setRef={c => this._setRef('two', c)}
                                            submitEditingHandler={() => this._focusNextField('three')}
                                        />
                                    </View>
                                    <View style={styles.formInfo}>
                                        <Text style={styles.infoText}>%</Text>
                                    </View>
                                </View>

                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Kadar Oksigen</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={oksigen}
                                            changeEvent={val => this.setState({ oksigen: val })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'done',
                                                blurOnSubmit: true,
                                                keyboardType: 'numeric',
                                            }}
                                            setRef={c => this._setRef('three', c)}
                                        />
                                    </View>
                                    <View style={styles.formInfo}>
                                        <Text style={styles.infoText}>ppm</Text>
                                    </View>
                                </View>
                            </View>

                            {
                                typeof message === 'string' && message !== '' &&
                                <View style={styles.messageContainer}>
                                    <Message
                                        type={status ? 'success' : 'error'}
                                        message={message}
                                    />
                                </View>
                            }

                            {
                                typeof message === 'object' && message.length > 0 &&
                                <View style={styles.messageContainer}>
                                    {
                                        message.map(item => (
                                            <Message
                                                key={item}
                                                type={status ? 'success' : 'error'}
                                                message={item}
                                            />
                                        ))
                                    }
                                </View>
                            }

                            <View style={styles.buttonWrapper}>
                                <ButtonBlue
                                    text="Simpan"
                                    pressEvent={() => {
                                        Alert.alert('Konfirmasi', 'Kirim data kualitas air?', [
                                            { text: 'Cancel', onPress: () => { }, style: 'cancel' },
                                            { text: 'OK', onPress: () => this._submitHandler() },
                                        ]);
                                    }}
                                    isLoading={isLoading}
                                />
                            </View>
                        </View>

                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
        );
    }
}
