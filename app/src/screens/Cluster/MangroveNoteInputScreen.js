import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ScrollView, View, Text, StyleSheet, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';

import ContainerHOC from '../../core/ContainerHOC';
import InputText from '../../components/InputText';
import ButtonBlue from '../../components/ButtonBlue';
import Message from '../../components/Message';

import * as clustersActionCreators from '../../data/clusters/actions';
import * as clusters from '../../data/clusters';

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 20,
        paddingBottom: 40,
        paddingLeft: 15,
        paddingRight: 15,
    },
    formContainer: {
        marginTop: 20,
    },
    formInner: {
        backgroundColor: '#fff',
        padding: 20,
    },
    formGroup: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    formLabel: {
        flex: 1,
    },
    labelText: {
        color: '#1A5185',
        fontFamily: 'MyriadProRegular',
        fontSize: 16,
    },
    formControl: {
        flex: 1,
        paddingLeft: 15,
    },
    buttonWrapper: {
        marginTop: 10,
    },
    messageContainer: {
        marginTop: 10,
    },
});

@connect(
    state => ({
        state,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { center: 'Input Data Keterangan' } })
export default class ShrimpInputScreen extends Component {
    state = {
        ditanam: '',
        hidup: '',
        mati: '',
        disisipkan: '',
        status: false,
        message: '',
        isLoading: false,
    }

    inputs = {}

    _submitHandler() {
        const { navigation: { state } } = this.props,
            {
                ditanam, hidup, mati, disisipkan,
            } = this.state;

        this.setState({ isLoading: true });

        if (ditanam && ditanam.length > 0 && hidup && hidup.length > 0 && mati && mati.length > 0 && disisipkan && disisipkan.length > 0) {
            clusters.updateNote(Object.assign({}, {
                ditanam, hidup, mati, disisipkan,
            }, { noteId: 'params' in state && 'noteId' in state.params ? state.params.noteId : null }))
                .then((resolve) => {
                    if (resolve.status.toLowerCase() === 'ok') {
                        this.props.actions.getNote({ petakId: ('params' in state && 'petakId' in state.params ? state.params.petakId : null) });
                    }

                    this.setState({ status: true, message: 'Ubah Data Berhasil!', isLoading: false });
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e || 'data' in e || 'error' in e) {
                        error = e.message || e.data || e.error;
                    }

                    const message = typeof error === 'object' ? Object.values(error).reduce((prev, current) => [...prev, ...current], []) : error;

                    this.setState({ message, status: false, isLoading: false });
                });
        } else {
            this.setState({ status: false, message: 'Periksa data. Semua variabel masukan harus diisi.', isLoading: false });
        }
    }

    _setRef(key, com) {
        this.inputs[key] = com;
    }

    _focusNextField(id) {
        this.inputs[id].focus();
    }

    render() {
        const {
            ditanam, hidup, mati, disisipkan, status, message, isLoading,
        } = this.state;

        return (
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag"
            >
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={styles.container}>
                        <View style={styles.formContainer}>
                            <View style={styles.formInner}>

                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Ditanam</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={ditanam}
                                            changeEvent={value => this.setState({ ditanam: value })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'next',
                                                blurOnSubmit: false,
                                                keyboardType: 'numeric',
                                            }}
                                            setRef={c => this._setRef('one', c)}
                                            submitEditingHandler={() => this._focusNextField('two')}
                                        />
                                    </View>
                                </View>

                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Hidup</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={hidup}
                                            changeEvent={value => this.setState({ hidup: value })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'next',
                                                blurOnSubmit: false,
                                                keyboardType: 'numeric',
                                            }}
                                            setRef={c => this._setRef('two', c)}
                                            submitEditingHandler={() => this._focusNextField('three')}
                                        />
                                    </View>
                                </View>

                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Mati</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={mati}
                                            changeEvent={value => this.setState({ mati: value })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'next',
                                                blurOnSubmit: false,
                                                keyboardType: 'numeric',
                                            }}
                                            setRef={c => this._setRef('three', c)}
                                            submitEditingHandler={() => this._focusNextField('four')}
                                        />
                                    </View>
                                </View>

                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={styles.labelText}>Disisipkan</Text>
                                    </View>
                                    <View style={styles.formControl}>
                                        <InputText
                                            value={disisipkan}
                                            changeEvent={value => this.setState({ disisipkan: value })}
                                            placeholder="0"
                                            customProps={{
                                                returnKeyType: 'done',
                                                blurOnSubmit: true,
                                                keyboardType: 'numeric',
                                            }}
                                            setRef={c => this._setRef('four', c)}
                                        />
                                    </View>
                                </View>
                            </View>

                            {
                                typeof message === 'string' && message !== '' &&
                                <View style={styles.messageContainer}>
                                    <Message
                                        type={status ? 'success' : 'error'}
                                        message={message}
                                    />
                                </View>
                            }

                            {
                                typeof message === 'object' && message.length > 0 &&
                                <View style={styles.messageContainer}>
                                    {
                                        message.map(item => (
                                            <Message
                                                key={item}
                                                type={status ? 'success' : 'error'}
                                                message={item}
                                            />
                                        ))
                                    }
                                </View>
                            }

                            <View style={styles.buttonWrapper}>
                                <ButtonBlue
                                    text="Simpan"
                                    pressEvent={() => {
                                        Alert.alert('Konfirmasi', 'Ubah data keterangan mangrove?', [
                                            { text: 'Cancel', onPress: () => {}, style: 'cancel' },
                                            { text: 'OK', onPress: () => this._submitHandler() },
                                        ]);
                                    }}
                                    isLoading={isLoading}
                                />
                            </View>
                        </View>

                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
        );
    }
}
