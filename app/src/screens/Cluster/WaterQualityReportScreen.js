import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ScrollView, View, BackHandler } from 'react-native';
import { ActionButton } from 'react-native-material-ui';

import ContainerHOC from '../../core/ContainerHOC';
import ChartSection from '../../components/clusters/ChartSection';

import * as clusters from '../../data/clusters/index';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        marginBottom: 40,
        paddingLeft: 20,
        paddingRight: 20,
    },
    picker: {
        backgroundColor: 'white',
        marginBottom: 10,
    },
    chartSection: {
        marginTop: 15,
    },
    scroll: {
        marginBottom: 40,
    },
});

@connect(state => ({
    homeKey: state.clusters.homeKey,
    chart: state.clusters.chart,
}))
@ContainerHOC({ header: { center: 'Input Kualitas Air' } })
export default class WaterQualityReportScreen extends Component {
    state = {
        filterKey: [null, null, null],
        chart: [],
        rawChart: [],
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            const { state } = this.props.navigation,
            { backToDrawer } = this.props;

            if ('params' in state && state.params && 'routeFrom' in state.params) {
                backToDrawer();
            }
        });

        if ('params' in this.props.navigation.state) {
            this._prepareData(this.props.navigation.state.params.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.chart !== nextProps.chart) {
            this._prepareData(nextProps.chart);

            this.state.filterKey.map((item, index) => {
                if (item !== null) {
                    this._changeFilter(index, item);
                }
            });
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    _changeFilter(index, value) {
        const filterKey = [...this.state.filterKey],
        chart = JSON.parse(JSON.stringify(this.state.chart)),
        filterValue = value ? value.split('-') : value; // [chart_id, year, month]

        filterKey[index] = value;

        if (value) {
            clusters.getChartByPeriod({ chartId: filterValue[0], year: filterValue[1], month: filterValue[2] })
                .then((resolve) => {
                    const idx = chart.findIndex(item => item.id === resolve.id);
                    if (idx > -1) {
                        chart[idx] = {
                            ...chart[idx],
                            id: resolve.id,
                            type: resolve.type,
                            typeView: resolve.type_view,
                            period: resolve.periode,
                            summary: resolve.status_laporan,
                            chartData: resolve.chartdata,
                            chartLabel: resolve.chartlabel,
                            chartColor: resolve.chartcolor,
                            chartSeries: resolve.chartseries,
                        };

                        this.setState({ chart });
                    }
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e) {
                        error = e.message;
                    }

                    alert(error);
                });
        } else {
            const raw = JSON.parse(JSON.stringify(this.state.rawChart));
            const idx = raw.findIndex(item => item.chart_id === chart[index].id);

            if (idx > -1) {
                chart[idx] = {
                    id: raw[idx].chart_id,
                    type: raw[idx].type,
                    typeView: raw[idx].type_view,
                    period: raw[idx].periode_view,
                    summary: raw[idx].status_laporan,
                    chartData: raw[idx].chartdata,
                    chartLabel: raw[idx].chartlabel,
                    chartColor: raw[idx].chartcolor,
                    chartSeries: raw[idx].chartseries,
                    periodOptions: raw[idx].periode,
                };

                this.setState({ chart });
            }
        }

        this.setState({
            filterKey,
        });
    }

    _prepareData(data) {
        const filterByType = [];

        filterByType.push(data.find(item => item.type === 'ph'));
        filterByType.push(data.find(item => item.type === 'salinitas'));
        filterByType.push(data.find(item => item.type === 'oksigen'));

        if (filterByType.filter(item => item).length > 0) {
            const chart = filterByType.map(item => ({
                id: item.chart_id,
                type: item.type,
                typeView: item.type_view,
                period: item.periode_view,
                summary: item.status_laporan,
                chartData: item.chartdata,
                chartLabel: item.chartlabel,
                chartColor: item.chartcolor,
                chartSeries: item.chartseries,
                periodOptions: item.periode,
            }));

            this.setState({ rawChart: data, chart });
        }
    }

    render() {
        const { filterKey, chart } = this.state,
        { navigation } = this.props,
        inputParams = chart.length > 0 ? chart.map(item => ({ id: item.id, type: item.type })) : {};

        return (
            <View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.container}>
                        <View styles={styles.bodySection}>
                            {chart.length > 0 &&
                                chart.map((item, index) => (
                                    <View
                                        key={item.id}
                                        style={styles.chartSection}
                                    >
                                        <ChartSection
                                            title={item.typeView}
                                            period={item.period}
                                            summary={item.summary}
                                            data={item.chartData}
                                            label={item.chartLabel}
                                            color={item.chartColor}
                                            series={item.chartSeries}
                                            periodValue={filterKey[index]}
                                            periodOptions={item.periodOptions}
                                            periodChangeHandler={value => this._changeFilter(index, value)}
                                        />
                                    </View>
                                ))
                            }
                        </View>
                    </View>
                </ScrollView>
                <ActionButton
                    icon="create"
                    style={{
                        container: {
                            backgroundColor: '#072B70', position: 'absolute', bottom: 50, right: 10,
                        },
                    }}
                    onPress={() => navigation.navigate('WaterQualityInput', { petakId: 'params' in navigation.state ? navigation.state.params.petakId : null, data: inputParams })}
                />
            </View>
        );
    };
}
