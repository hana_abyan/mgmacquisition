import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, TextInput, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';

import ContainerHOC from '../core/ContainerHOC';
import ButtonBlue from '../components/ButtonBlue';
import Message from '../components/Message';

import * as users from '../data/users';

const styles = StyleSheet.create({
    scroll: {
        marginTop: 5,
        flexDirection: 'column',
    },
    container: {
        // flex: 1,
        marginTop: 20,
        paddingLeft: 15,
        paddingRight: 15,
    },
    inner: {
        backgroundColor: '#fff',
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 20,
        paddingBottom: 40,
    },
    formGroup: {
        flexDirection: 'column',
        marginTop: 15,
    },
    controlLabel: {
        flex: 1,
        color: '#084071',
        fontFamily: 'MyriadProRegular',
        fontSize: 22,
    },
    formControl: {
        flex: 1,
        flexDirection: 'row',
        height: 40,
        marginTop: 10,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#E3E3E3',
        backgroundColor: '#fff',
        color: '#777',
        paddingLeft: 20,
        paddingRight: 20,
        fontFamily: 'MyriadProRegular',
    },
    messageContainer: {
        marginTop: 10,
    },
    buttonWrapper: {
        marginTop: 10,
    },
});

@ContainerHOC({ header: { center: 'Ubah Password' } })
export default class ChangePasswordScreen extends Component {
    state = {
        currentPassword: '',
        newPassword: '',
        newPasswordConfirm: '',
        message: '',
        status: false,
        isLoading: false,
    }

    inputs = {}

    _submitHandler() {
        this.setState({ isLoading: true });
        const { currentPassword, newPassword, newPasswordConfirm } = this.state;

        if (String(currentPassword).length <= 0 || String(newPassword).length <= 0 || String(newPasswordConfirm).length <= 0) {
            this.setState({ status: false, message: 'Isikan password saat ini dan password baru', isLoading: false });
        } else {
            users.changePassword({
                _method: 'PATCH',
                old_password: currentPassword,
                new_password: newPassword,
                new_password_confirmation: newPasswordConfirm,
            })
                .then((resolve) => {
                    if (resolve.status.toLowerCase() === 'ok') {
                        this.setState({
                            message: resolve.message,
                            status: true,
                            isLoading: false,
                            currentPassword: '',
                            newPassword: '',
                            newPasswordConfirm: '',
                        });
                    }
                })
                .catch((e) => {
                    let error = 'Unknown Error';

                    if ('message' in e || 'data' in e || 'error' in e) {
                        error = e.message || e.data || e.error;
                    }

                    const message = (typeof error === 'object') ? Object.values(error).reduce((prev, current) => [...prev, ...current], []) : error;

                    this.setState({ message, status: false, isLoading: false });
                });
        }
    }

    _setRef(key, com) {
        this.inputs[key] = com;
    }

    _focusNextField(id) {
        this.inputs[id].focus();
    }

    render() {
        const {
            currentPassword, newPassword, newPasswordConfirm, message, status, isLoading,
        } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag"
                >
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View
                            style={styles.container}
                        >
                            <View
                                style={styles.inner}
                            >
                                <View style={styles.formGroup}>
                                    <Text
                                        style={styles.controlLabel}
                                    >Password saat ini
                                    </Text>
                                    <TextInput
                                        value={currentPassword}
                                        onChangeText={val => this.setState({ currentPassword: val })}
                                        style={styles.formControl}
                                        underlineColorAndroid="transparent"
                                        autoCorrect={false}
                                        returnKeyType="next"
                                        blurOnSubmit={false}
                                        autoCapitalize="none"
                                        secureTextEntry
                                        ref={c => this._setRef('one', c)}
                                        onSubmitEditing={() => this._focusNextField('two')}
                                    />
                                </View>

                                <View style={styles.formGroup}>
                                    <Text
                                        style={styles.controlLabel}
                                    >Password baru
                                    </Text>
                                    <TextInput
                                        value={newPassword}
                                        onChangeText={val => this.setState({ newPassword: val })}
                                        style={styles.formControl}
                                        underlineColorAndroid="transparent"
                                        autoCorrect={false}
                                        returnKeyType="next"
                                        blurOnSubmit={false}
                                        autoCapitalize="none"
                                        secureTextEntry
                                        ref={c => this._setRef('two', c)}
                                        onSubmitEditing={() => this._focusNextField('three')}
                                    />
                                </View>

                                <View style={styles.formGroup}>
                                    <Text
                                        style={styles.controlLabel}
                                    >Ketik ulang password baru
                                    </Text>
                                    <TextInput
                                        value={newPasswordConfirm}
                                        onChangeText={val => this.setState({ newPasswordConfirm: val })}
                                        style={styles.formControl}
                                        underlineColorAndroid="transparent"
                                        autoCorrect={false}
                                        returnKeyType="done"
                                        blurOnSubmit
                                        autoCapitalize="none"
                                        secureTextEntry
                                        ref={c => this._setRef('three', c)}
                                    />
                                </View>
                            </View>

                            {
                                typeof message === 'string' && message !== '' &&
                                <View style={styles.messageContainer}>
                                    <Message
                                        type={status ? 'success' : 'error'}
                                        message={message}
                                    />
                                </View>
                            }

                            {
                                typeof message === 'object' && message.length > 0 &&
                                <View style={styles.messageContainer}>
                                    {
                                        message.map(item => (
                                            <Message
                                                key={item}
                                                type={status ? 'success' : 'error'}
                                                message={item}
                                            />
                                        ))
                                    }
                                </View>
                            }

                            <View
                                style={styles.buttonWrapper}
                            >
                                <ButtonBlue
                                    text="Simpan"
                                    pressEvent={() => {
                                        Alert.alert('Konfirmasi', 'Ubah password?', [
                                            { text: 'Cancel', onPress: () => {}, style: 'cancel' },
                                            { text: 'OK', onPress: () => this._submitHandler() },
                                        ]);
                                    }}
                                    isLoading={isLoading}
                                    dark
                                />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </View>
        );
    }
}
