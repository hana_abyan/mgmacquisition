import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView, View, Text, StyleSheet, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

import ContainerHOC from '../core/ContainerHOC';
import InputSearch from '../components/InputSearch';

import * as forumsActionCreators from '../data/forums/actions';
import * as forums from '../data/forums';

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    headerTitleText: {
        color: '#154B78',
        fontSize: 23,
        alignSelf: 'flex-start',
        marginTop: 20,
    },
    categoryContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        marginTop: 10,
    },
    category: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        backgroundColor: '#fff',
    },
    wrapper: {
        flex: 1,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#eee',
    },
    inner: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    image: {
        width: 45,
        height: 45,
    },
    title: {
        flex: 1,
        color: '#2C4D7D',
        fontSize: 21,
        fontWeight: '500',
        marginLeft: 25,
    },
});

const ForumCategory = ({ text, icon, pressEvent }) => (
    <TouchableOpacity
        style={styles.wrapper}
        onPress={() => pressEvent()}
        activeOpacity={0.5}
    >
        <View style={styles.inner}>
            <Image
                source={{ uri: icon }}
                style={styles.image}
            />
            <Text
                style={styles.title}
            >{text}
            </Text>
        </View>
    </TouchableOpacity>
);

@connect(
    state => ({
        categories: state.forums.categories,
    }),
    dispatch => ({
        actions: bindActionCreators(forumsActionCreators, dispatch),
    }),
)
@ContainerHOC({ header: { left: 'menu', center: 'Forum', destination: 'DrawerToggle' } })
export default class ForumScreen extends Component {
    static propTypes = {
        navigation: PropTypes.shape({
            navigate: PropTypes.func.isRequired,
        }).isRequired,
    };

    state = {
        searchKey: '',
        data: [],
        isLoading: false,
        message: '',
    }

    componentWillMount() {
        this.props.actions.getCategory()
            .then((resolve) => {
                this._setData(resolve);
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                alert(error);
            });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.categories !== nextProps.categories) {
            this._setData(nextProps.categories);
        }
    }

    _setData(data) {
        this.setState({ data });
    }

    _navigationHandler(categoryId, categoryName) {
        const { navigation } = this.props;

        this.props.actions.getThread({ categoryId })
        .then((resolve) => {
                this._resetSearch();
                navigation.navigate('ForumThread', { data: resolve, categoryId, categoryName });
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                alert(error);
            });
    }

    _searchHandler() {
        const { searchKey } = this.state,
        { navigation } = this.props;

        this.setState({ isLoading: true });

        forums.postSearch({ value: searchKey })
            .then((resolve) => {
                let message = '';

                if (resolve.length === 0) {
                    message = `Tidak dapat menemukan pencarian dengan kata kunci ${searchKey}`;
                } else {
                    navigation.navigate('ForumThread', { data: resolve, searchKey });
                }

                this.setState({ message, isLoading: false });
                // navigation.navigate('ForumThread', { data: resolve, categoryId, categoryName });
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                this.setState({ isLoading: false });
                alert(error);
            });
    }

    _resetSearch() {
        this.setState({ searchKey: '', message: '' });
    }

    render() {
        const { navigation, categories } = this.props,
        {
            searchKey, data, isLoading, message,
        } = this.state;
        return (
            <ScrollView style={styles.scroll}>
                <View style={styles.container}>
                    <InputSearch
                        placeholder="Cari forum..."
                        changeEvent={val => this.setState({ searchKey: val })}
                        value={searchKey}
                        onSubmit={() => this._searchHandler()}
                        isLoading={isLoading}
                    />
                    {
                        message.length > 0 &&
                        <Text>{message}</Text>
                    }

                    <Text style={styles.headerTitleText}>Kategori Forum</Text>

                    <View style={styles.categoryContainer}>
                        {
                            data.length > 0 &&
                            data.map(item => (
                                <View
                                    style={styles.category}
                                    key={`forum-category-${item.category_forum_id}`}
                                >
                                    <ForumCategory
                                        text={item.category_forum_name}
                                        icon={item.category_icon}
                                        pressEvent={() => this._navigationHandler(item.category_forum_id, item.category_forum_name)}
                                    />
                                </View>
                            ))
                        }
                    </View>
                </View>
            </ScrollView>
        );
    }
}
