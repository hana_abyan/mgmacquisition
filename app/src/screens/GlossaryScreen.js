import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ScrollView, View, StyleSheet, Picker } from 'react-native';

import ContainerHOC from '../core/ContainerHOC';
import SectionList from '../components/glossaries/SectionList';

import * as glossariesActionCreators from '../data/glossaries/actions';

const styles = StyleSheet.create({
    scroll: {
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 50,
        paddingLeft: 15,
        paddingRight: 15,
    },
    inner: {
        backgroundColor: '#fff',
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
    },
    picker: {
        backgroundColor: 'white',
        marginBottom: 10,
    },
    titleSection: {
        color: '#12466E',
        fontSize: 30,
        paddingTop: 20,
        marginBottom: 20,
        fontWeight: '500',
    },
});

@connect(
    state => ({
        glossaries: state.glossaries.list,
    }),
    dispatch => ({
        actions: {
            glossaries: bindActionCreators(glossariesActionCreators, dispatch),
        },
    }),
)
@ContainerHOC({ header: { left: 'menu', center: 'Istilah', destination: 'DrawerToggle' } })
export default class GlossaryScreen extends Component {
    state = {
        list: [],
        dataSection: [],
        filterKey: '',
        filteredData: [],
    };

    componentWillMount() {
        this.props.actions.glossaries.get();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.glossaries !== nextProps.glossaries) {
            this._setData(nextProps.glossaries);
        }
    }

    _setData(data) {
        this.setState({
            list: data,
            filteredData: data,
        });
    }

    _filterData() {
        const { filterKey, list } = this.state;

        if (filterKey) {
            const filteredData = {};
            filteredData[filterKey] = list[filterKey];
            this.setState({ filteredData });
        } else {
            this.setState({ filteredData: list });
        }
    }

    _changeFilter(filterKey) {
        this.setState({ filterKey }, () => this._filterData());
    }

    render() {
        const { filteredData, filterKey, list } = this.state;
        const pickerItem = [];
        pickerItem.push(<Picker.Item key="none" label="Pilih Semua" value="" />);
        const keyList = Object.keys(list);
        const keyFilteredList = Object.keys(filteredData);

        if (keyList.length > 0) {
            keyList.map(item => pickerItem.push(<Picker.Item key={item} label={item} value={item} />));
        }

        return (
            <ScrollView
                style={styles.scroll}
            >
                <View style={styles.container}>
                    <Picker
                        selectedValue={filterKey}
                        onValueChange={value => this._changeFilter(value)}
                        style={styles.picker}
                    >
                        {pickerItem}
                    </Picker>

                    <View style={styles.inner}>
                        {
                            keyFilteredList.length > 0 &&
                            keyFilteredList.map(item => (
                                <SectionList
                                    key={`gs-${item}`}
                                    header={item}
                                    data={list[item]}
                                />
                            ))
                        }
                    </View>
                </View>
            </ScrollView>
        );
    }
}
