import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { KeyboardAvoidingView, StyleSheet, View, Image, Text, Keyboard, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../components/InputText';
import ButtonBlue from '../components/ButtonBlue';

import * as session from '../services/session';
import * as clustersActionCreators from '../data/clusters/actions';

const logo = require('../assets/images/logo_mgm.png');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 50,
        paddingRight: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoImage: {
        width: 150,
        height: 150,
        marginBottom: 30,
    },
    error: {
        color: 'red',
    },
});

@connect(
    state => ({
        state,
    }),
    dispatch => ({
        actions: bindActionCreators(clustersActionCreators, dispatch),
    }),
)
export default class LoginScreen extends Component {
    static propTypes = {
        actions: PropTypes.shape({
            getMenu: PropTypes.func,
        }),
        onLogin: PropTypes.func,
    }

    static defaultProps = {
        actions: {
            getMenu: () => {},
        },
        onLogin: () => {},
    }

    state = { ...this.initialState };

    initialState = {
        username: '',
        password: '',
        error: '',
        isLoading: false,
    };

    inputs = {};

    _submitHandler() {
        this.setState({ isLoading: true });
        Keyboard.dismiss();

        const { username, password } = this.state;

        session.authenticate(username, password)
            .then(() => {
                this.props.actions.getMenu();
                this.setState(this.initialState);
                this.props.onLogin();
            })
            .catch((e) => {
                let error = 'Unknown Error';

                if ('message' in e) {
                    error = e.message;
                }

                this.setState({ error, isLoading: false });
            });
    }

    _setRef(key, com) {
        this.inputs[key] = com;
    }

    _focusNextField(id) {
        this.inputs[id].focus();
    }

    render() {
        const { username: stUsername, password: stPassword, error, isLoading } = this.state;

        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <KeyboardAvoidingView
                    style={styles.container}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag"
                    // behavior="padding"
                    // keyboardVerticalOffset={-300}
                >
                    <Image source={logo} style={styles.logoImage} />
                    <InputText
                        placeholder="username"
                        changeEvent={username => this.setState({ username })}
                        value={stUsername}
                        customProps={{
                            autoCorrect: false,
                            returnKeyType: 'next',
                            autoCapitalize: 'none',
                            blurOnSubmit: false,
                        }}
                        setRef={c => this._setRef('one', c)}
                        submitEditingHandler={() => this._focusNextField('two')}
                    />

                    <InputText
                        placeholder="password"
                        changeEvent={password => this.setState({ password })}
                        value={stPassword}
                        customProps={{
                            secureTextEntry: true,
                            autoCorrect: false,
                            returnKeyType: 'done',
                            autoCapitalize: 'none',
                            blurOnSubmit: true,
                        }}
                        setRef={c => this._setRef('two', c)}
                    />
                    <ButtonBlue
                        text="MASUK"
                        pressEvent={e => this._submitHandler(e)}
                        isLoading={isLoading}
                    />

                    {error !== '' &&
                        <View>
                            <Text style={styles.error}>{error}</Text>
                        </View>
                    }
                </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
        );
    }
}
