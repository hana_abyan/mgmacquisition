import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, ScrollView, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

import ContainerHOC from '../core/ContainerHOC';
import InputSearch from '../components/InputSearch';
import CategoryBox from '../components/CategoryBox';

import * as InfoActionCreators from '../data/articles/actions';

const Image1 = require('../assets/images/article-img1.jpg');
const Image2 = require('../assets/images/article-img2.png');

const styles = StyleSheet.create({
    scroll: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingBottom: 40,
    },
    searchContainer: {
        marginTop: 20,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
    },
    categorContainer: {
        marginTop: 20,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#EEEEEE',
        paddingTop: 10,
        paddingBottom: 20,
    },
    items: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    articleContainer: {
        marginTop: 10,
        paddingLeft: 15,
        paddingRight: 15,
    },
    articleWrapper: {
        marginTop: 10,
    },
    articleItem: {
        backgroundColor: '#fff',
        padding: 10,
        paddingBottom: 30,
        borderColor: '#E6E9E9',
        borderWidth: 1,
    },
    imageWrapper: {
        maxHeight: 300,
        overflow: 'hidden',
    },
    articleImage: {
        borderWidth: 1,
        width: '100%',
        height: 300,
    },
    articleTitle: {
        marginTop: 7,
        color: '#155386',
        fontSize: 20,
        fontWeight: '500',
    },
    articleSubTitle: {
        fontSize: 14,
        marginTop: 4,
    },
    preview: {
        marginTop: 17,
        fontSize: 18,
    },
    buttonWrapper: {
        marginTop: 30,
        borderColor: '#155386',
        borderWidth: 1,
        borderRadius: 3,
        paddingTop: 8,
        paddingBottom: 8,
    },
    buttonText: {
        color: '#155386',
        fontSize: 20,
        alignSelf: 'center',
    },
});

const categoryData = [
    {
        id: 1,
        name: 'Semua Kategori',
    },
    {
        id: 2,
        name: 'Budidaya',
    },
    {
        id: 3,
        name: 'Berita',
    },
    {
        id: 4,
        name: 'Tambak',
    },
];

const articleData = [
    {
        id: 1,
        image: Image1,
        title: 'Cara Budidaya Udang Vaname dengan Berbagi Teknik Lengkap Beserta Tahapannya',
        category: 'Tambak',
        createdAt: '12 September 2017',
        preview: 'Saat ini banyak sekali usaha yang mengandalkan pada sektor budidaya. Nah kali ini yang akan membahas salah satu sektor budidaya yang cukup besar penghasilannya, yaitu cara budidaya udang vaname',
        content: 'Saat ini banyak sekali usaha yang mengandalkan pada sektor budidaya. Nah kali ini yang akan membahas salah satu sektor budidaya yang cukup besar penghasilannya, yaitu cara budidaya udang vaname. Budidaya udang vaname adalah bisnis yang sangat menjanjikan jika ditekuni dengan sungguh-sungguh. Kini masyarakat yang mulai terjun dalan dunia budidaya mulai melirik usaha ini dan mencari bagaimana cara budidaya udang vaname secara tradisional. Udang vaname ini memiliki nilai jual yang sangat tinggi karena rasanya yang enak serta cukup kuat dari serangan penyakit serta cuaca yang tidak menentu. Cara budidaya udang vaname secara tradisional ini sangat cocok bagi para pemula. Sebelum kalian terjun langsung dan mencoba budidaya ini, simak beberapa hal penting yang harus diperhatikan cara yang baik dan benar. Habitat Udang Vaname Pada dasarnya udang adalah hewan yang hidup di perairan seperti di laut, sungai serta danau. Udang bisa ditemukan dengan mudah pada genangan air yang berukuran besar seperti di air tawar, air payau dan air asin dengan tingkat kedalaman yang bervariasi pula. Udang Vaname adalah salah satu dari sekian banyak jenis udang yang belakangan ini sering diminati dan menjadi buruan masyarakat.Terdapat beberapa keunggulan dari udang ini yakni masa pertumbuhan yang cepat, tahan dari serangan penyakit. Pembudidaya udang menganggap bahwa udang ini hanya bisa dibudidayakan secara intensif.Namun hal ini salah total, ternyata udang vaname pun bisa dibudidayakan dencan pola tradisional.Bahkan pola tradisional dapat lebih baik daripada pola intensif. Budidaya Udang Vaname Secara Tradisional Pola tradisional juga lebih menguntungkan karena dapat panen dalam jumlah yang lebih banyak.Teknologi yang ada saat ini hanya tersedia untuk pola intesif.Luas tambak di Indonesia bisa mencapai sekitar 360.000 ha dan digarap oleh petambak yang masih kurang mampu.Informasi dalam mengolah udang vaname secara tradisional masih sangat terbatas hingga saat ini.Maka berikut ini akan dijelaskan dengan rinci cara budidaya udang vaname;',
    },
    {
        id: 2,
        image: Image2,
        title: 'Cara Budidaya Udang Vaname Untuk Hasil Yang Terbaik',
        category: 'Tambak',
        createdAt: '12 September 2017',
        preview: 'Udang vaname adalah udang yang berasal dari kawasan sub tropis. Akan tetapi, karena daya tahan udang ini yang cukup hebat, udang ini juga dapat dikembangkan di daerah tropis seperti Indonesia dengan teknik budidayayang tepat. Ada beberapa tips yang bisa anda gunakan untuk membudidayakan udang ini',
        content: 'Persiapan Tambak Pertama dilakukan proses pengeringan tambak selama 7-10 hari sampai tanah terlihat pecah-pecah untuk memutus siklus hidup pathogen dan mengurai gas beracun H2S. Setelah itu, dilakukan proses pembalikan tanah agar fitoplankton dapat tumbuh sebagai pakan alami udang vaname. Perlu juga dilakukan pengukuran pH tanah. Apabila pH kurang dari 6,5, maka perlu dilakukan proses pengapuran. Pemupukan dan Pengisian Air Pemupukan dilakukan setelah proses pengeringan dan pengapuran. Pupuk yang digunakan adalah pupuk Urea 150 kg/ha dan pupuk kandang 2000 kg/ha. Setelah itu, dilakukan pengisian air dengan kedalaman 1 m atau kurang di petak pembesaran. Biarkan air selama 2-3 minggu sampai siap untuk proses selanjutnya yaitu penebaran bibit udang vaname. Pemilihan Benih Benih yang digunakan dalam cara budidaya udang vaname ini adalah benih jenis PL10-PL12 yang mendapatkan sertifikasi SPF (Specific Pathogen Free). Benih harus tampak bagus tanpa cacat, mempunyai ukuran seragam, berenang melawan arus, insang sudah berkembang, dan usus terlihat jelas. Penebaran Benih Sebelum ditebar, benih udang vaname perlu melalui proses aklimitasi, karena, hal ini sangat berpengaruh pada daya tahan udang ini saat proses pembenihan dan pemeliharaan. Caranya, menyiram kantung tempat benih dengan air tambak dan diapungkan ditambak selama 15-20 menit. Setelah itu, dibuka dan dimiringkan pelan-pelan agar benih udang keluar. Tidak seperti cara beternak udang lainnya, benih udang vaname sebaiknya ditebar pada siang hari. Pemberian Pakan Pakan yang biasa dianjurkan pada panduan cara ternak udang di Indonesia adalah pellet yang mengandung 30% protein. Jumlah pakan yang diberikan dipengaruhi oleh umur udang atau menggunakan pedoman ABW. Pemberian pakan dilakukan sebanyak 4-5 kali sehari. Selain umur, banyaknya pakan dipengaruhi oleh kondisi tanah tambak, kualitas air dan tingkat kesehatan udang. Pemeliharaan Langkah pemeliharaan pertama adalah kontrol tingkat salinitas. Salinitas air yang baik adalah 10-25 ppt. Selain itu pemeriksaan pH air dan tanah secara berkala. Bila kurang dari 7,5, maka perlu dilakukan proses pengapuran tambahan.Sebelum udang berumur 60 hari, perlu juga diperiksa tinggi air dan dilakukan pengisian air dengan salinitas yang disebutkan diatas bila air kurang karena proses penguapan. Pengendalian Hama Hama yang menyerang tambak udang vaname biasanya adalah hewan-hewan yang hidup disekitar tambak, seperti burung, ketam, ikan liar dan pengerek. Untuk ketam dan pengerek yang biasanya melubangi pematang disekitar tambak, kita bisa memasang pagar plastik untuk mencegah hewan ini masuk. Ikan liar bisa dibasmi dengan saponin. Dan burung, kita perlu mengontrol tambak sesering mungkin. Pengendalian Penyakit Pengendalian penyakit yang tepat dilakukan bersamaan dengan proses pembibitan dan pemeliharaan. Bila kita melakukan proses pemeliharaan dengan baik, maka penyakit tidak akan menyerang udang kita. Selain itu, kita juga perlu melakukan pemeriksaan fisik udang dan tes Polymerase Chain Reaction (PCR) dilaboratorium. Pemanenan Proses pemanenan dilakukan setelah udang vaname berumur 120 hari dan mencapai berat, yaitu 50 ekor/kg. Bila udang sudah mencapai berat tersebut sebelum 120 hari, maka pemanenan bisa dilakukan. Pemanenan dilakukan pada waktu malam hari untuk mempertahankan kualitas udang. 2-4 hari sebelum pemanenan, tambak diberi kapur dolomite 80 kg/ha dan mempertahankan ketinggian air untuk mencegah proses molting. Bila kita melakukan teknik beternak udang vaname dengan benar, maka hasil yang kita dapatkan akan sangat memuaskan. Sumber: http://1001budidaya.com/budidaya-udang-vaname/',
    },
];

const ArticleItem = ({ image, title, category, createdAt, preview, pressEvent }) => (
    <View style={styles.articleItem}>
        <View style={styles.imageWrapper}>
            <Image
                resizeMode="contain"
                style={styles.articleImage}
                source={{ uri: image }}
            />
        </View>
        <Text style={styles.articleTitle}>{title}</Text>
        <Text style={styles.articleSubTitle}>{`${category} | ${createdAt}`}</Text>
        <Text style={styles.preview}>{preview}</Text>

        <TouchableOpacity
            style={styles.buttonWrapper}
            activeOpacity={0.7}
            onPress={() => pressEvent()}
        >
            <Text style={styles.buttonText}>Baca lebih lanjut</Text>
        </TouchableOpacity>
    </View>
);

@connect(state => ({
    articles: state.articles.list,
    detail: state.articles.detail,
    categories: state.articles.category,
}), dispatch => ({
    actions: {
        info: bindActionCreators(InfoActionCreators, dispatch),
    },
}))
@ContainerHOC({ header: { left: 'menu', center: 'Info', destination: 'DrawerToggle' } })
export default class InfoScreen extends Component {
    state = {
        filterKey: '',
        activeCategory: '',
        isLoading: false,
        isSearching: false,
    }

    componentWillMount() {
        this.props.actions.info.getAll();
        this.props.actions.info.getCategory();
    }

    _getArticleByCategory(id) {
        this.setState({
            activeCategory: id,
            isLoading: true,
            isSearching: false,
            filterKey: '',
        });

        if (id === '') {
            this.props.actions.info.getAll().then(resolve => resolve).catch(e => e).then(() => this.setState({
                isLoading: false,
            }));
        } else {
            this.props.actions.info.getByCategory({ categoryId: id }).then(resolve => resolve).catch(e => e).then(() => this.setState({
                isLoading: false,
            }));
        }
    }

    _searchHandler() {
        const { filterKey } = this.state;

        this.setState({ isSearching: true, isLoading: true, activeCategory: '' });

        if (filterKey === '') {
            this.props.actions.info.getAll().then(resolve => resolve).catch(e => e).then(() => this.setState({
                isLoading: false,
            }));
        } else {
            this.props.actions.info.postSearch({ value: filterKey }).then(resolve => resolve).catch(e => e).then(() => this.setState({
                isLoading: false,
            }));
        }
    }

    _navigateHandler(articleId) {
        const { navigation } = this.props,
        detailUri = 'http://mandiri.shideichis.me/apps/articles/:articleId';

        navigation.navigate('InfoDetail', { uri: detailUri.replace(':articleId', articleId) });
    }

    render() {
        const { filterKey, activeCategory, isLoading, isSearching } = this.state,
            { articles, categories } = this.props,
            categoriesWithAll = [{ id: '', name: 'Semua Kategori' }, ...categories];

        return (
            <ScrollView style={styles.scroll}>
                <View style={styles.container}>
                    <View style={styles.searchContainer}>
                        <InputSearch
                            placeholder="Cari artikel"
                            value={filterKey}
                            changeEvent={value => this.setState({ filterKey: value })}
                            onSubmit={() => this._searchHandler()}
                        />
                    </View>
                    <View style={styles.categorContainer}>
                        <CategoryBox
                            data={categoriesWithAll}
                            activeId={activeCategory}
                            pressEvent={id => this._getArticleByCategory(id)}
                        />
                    </View>

                    <View style={styles.articleContainer}>
                        {
                            articles.length > 0 && !isLoading &&
                            articles.map(item => (
                                <View
                                    key={`article-item-${item.id}`}
                                    style={styles.articleWrapper}
                                >
                                    <ArticleItem
                                        image={item.cover_image}
                                        title={item.title}
                                        category={item.category}
                                        createdAt={item.publish_date_view}
                                        preview={item.short_desc}
                                        pressEvent={() => this._navigateHandler(item.id)}
                                    />
                                </View>
                            ))
                        }

                        {
                            articles && articles.length === 0 && activeCategory === '' && !isLoading && !isSearching &&
                            <Text>Tidak ada artikel untuk semua kategori.</Text>
                        }

                        {
                            articles && articles.length === 0 && activeCategory !== '' && !isLoading && !isSearching &&
                            <Text>Tidak ada artikel untuk kategori {categoriesWithAll.find(item => item.id === activeCategory).name}.</Text>
                        }

                        {
                            articles && articles.length === 0 && isSearching && !isLoading && filterKey !== '' &&
                            <Text>{filterKey} tidak ditemukan dalam pencarian.</Text>
                        }

                        {
                            isLoading &&
                            <View style={{ marginTop: 30, alignSelf: 'center' }}>
                                <ActivityIndicator size="large" color="#1A537C" />
                            </View>
                        }
                    </View>
                </View>
            </ScrollView>
        );
    }
}
