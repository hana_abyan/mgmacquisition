export const zeroAdd = (val) => {
    if (String(val).length === 1) {
        return `0${val}`;
    }

    return val;
};
