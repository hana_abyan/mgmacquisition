import { COLOR } from 'react-native-material-ui';

export const uiTheme = {
    palette: {
        primaryColor: '#154B78',
        accentColor: COLOR.pink500,
    },
    toolbar: {
        container: {
            backgroundColor: 'white'
        },
        titleText: {
            color: '#154B78',
        },
        leftElement: {
            color: '#154B78'
        }
    },
    avatar: {
        container: {
            backgroundColor: '#333',
        },
    },
};

export const assetBaseURI = 'http://mandiri.shideichis.me/storage/';
