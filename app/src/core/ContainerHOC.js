import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, StatusBar } from 'react-native';
import { NavigationActions } from 'react-navigation';
import Dimensions from 'Dimensions';
import { ThemeProvider, Toolbar } from 'react-native-material-ui';
import { uiTheme } from './config';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: '#F5F5F3',
        flex: 1,
    },
});

const titleFormater = (text) => {
    const arrText = text.split(' ');

    if (arrText.length > 1) {
        return `${arrText[0]} ${arrText[1].toUpperCase()}`;
    }

    return text;
};

const ContainerHOC = custom => WrappedComponent => (props) => {
    const mergedStyle = (custom && Object.hasOwnProperty.call(custom, 'style')) ? [styles.containerStyle, custom.style] : styles.containerStyle;
    const { header = null } = custom || {};
    const { navigation: { state: routeState } } = props;

    let headerCenter = '';
    if (Object.prototype.hasOwnProperty.call(routeState, 'params')) {
        if ((routeState.routeName === 'ClusterMenu' || routeState.routeName === 'MangroveMenu' || routeState.routeName === 'PictureGallery') && Object.prototype.hasOwnProperty.call(routeState.params, 'name')) {
            headerCenter = titleFormater(routeState.params.name);
        }

        if (routeState.routeName === 'PictureGallery') {
            headerCenter = `Foto ${headerCenter}`;

            // if (routeState.params.type === 'Swath') {
            //     header.destination = 'ClusterMenu';
            // } else {
            //     header.destination = 'MangroveMenu';
            // }
        }
    }

    // const backAction = NavigationActions.back({ key: routeState.key });
    // console.log(routeState);
    // console.log(routeState.key);
    // console.log(console.log(header.key));
    const _backFromDrawer = (fromNavigator) => {
        if (fromNavigator) {
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Home' }),
                ],
            });

            props.navigation.dispatch(resetAction);
            props.navigation.goBack(null);
        }
        props.navigation.goBack(null);
    };

    return (
        <ThemeProvider uiTheme={uiTheme}>
            <View
                style={mergedStyle}
            >
                <StatusBar backgroundColor="rgba(21, 75, 120, 0.8)" />
                <Toolbar
                    leftElement={(header && header.left) || 'arrow-back'}
                    onLeftElementPress={() => {
                        if (header && header.destination) {
                            props.navigation.navigate((header.destination));
                        } else {
                            // if (Object.prototype.hasOwnProperty.call(routeState, 'params') && Object.prototype.hasOwnProperty.call(routeState.params, 'routeFrom')) {
                            //     console.log(props.homeKey);
                            //     navigation.dispatch();
                            // }
                            // // props.navigation.goBack(Object.prototype.hasOwnProperty.call(routeState, 'params') && Object.prototype.hasOwnProperty.call(routeState.params, 'routeFrom') ? props.homeKey : null);
                            if ('params' in routeState && routeState.params && 'routeFrom' in routeState.params) {
                                // props.navigation.goBack(props.homeKey);
                                // let goBack = NavigationActions.back({ key: props.homeKey });
                                // props.navigation.dispatch(goBack);
                                _backFromDrawer(true);

                                // props.navigation.navigate('Home');
                            }else {
                                props.navigation.goBack(null);
                            }
                        }
                    }}
                    centerElement={(header && header.center) || headerCenter}
                />
                <WrappedComponent
                    {...props}
                    windowWidth={DEVICE_WIDTH}
                    windowHeight={DEVICE_HEIGHT}
                    backToDrawer={() => _backFromDrawer()}
                />
            </View>
        </ThemeProvider>
    );
};

export default ContainerHOC;
