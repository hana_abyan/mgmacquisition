import { Dimensions, Platform } from 'react-native';
import { DrawerNavigator, StackNavigator } from 'react-navigation';

import CustomDrawer from '../components/layouts/CustomDrawer';

import HomeScreen from '../screens/HomeScreen';
import ClusterMenu from '../screens/Cluster/MenuScreen';
import MangroveMenuScreen from '../screens/Cluster/MangroveMenuScreen';
import UpdateProgressScreen from '../screens/Cluster/ProgressScreen';
import WaterQualityReportScreen from '../screens/Cluster/WaterQualityReportScreen';
import WaterQualityInputScreen from '../screens/Cluster/WaterQualityInputScreen';
import ShrimpReportScreen from '../screens/Cluster/ShrimpReportScreen';
import ShrimpInputScreen from '../screens/Cluster/ShrimpInputScreen';
import PictureGalleryScreen from '../screens/Cluster/PictureGalleryScreen';
import PictureDetailScreen from '../screens/Cluster/PictureDetailScreen';
import PictureTakerScreen from '../screens/Cluster/PictureTakerScreen';

import MangroveGrowthReportScreen from '../screens/Cluster/MangroveGrowthReportScreen';
import MangroveGrowthInputScreen from '../screens/Cluster/MangroveGrowthInputScreen';
import MangroveNoteReportScreen from '../screens/Cluster/MangroveNoteReportScreen';
import MangroveNoteInputScreen from '../screens/Cluster/MangroveNoteInputScreen';

import InfoScreen from '../screens/InfoScreen';
import InfoDetailScreen from '../screens/InfoDetailScreen';
import ForumScreen from '../screens/ForumScreen';
import ForumThreadScreen from '../screens/Forum/ForumThreadScreen';
import ForumDetailScreen from '../screens/Forum/ForumDetailScreen';
import ForumCreateThreadScreen from '../screens/Forum/ForumCreateThreadScreen';

import GlossaryScreen from '../screens/GlossaryScreen';
import ChangePasswordScreen from '../screens/ChangePasswordScreen';

const drawerWidth = Dimensions.get('window').width - (Platform.OS === 'android' ? 86 : 94);

const ForumThreadStack = StackNavigator({
    ForumThread: {
        screen: ForumThreadScreen,
    },
    ForumDetail: {
        screen: ForumDetailScreen,
        // path: 'forumDetail/:payload',
    },
    ForumThreadNew: {
        screen: ForumCreateThreadScreen,
    },
}, {
    headerMode: 'none',
});

const ForumStack = StackNavigator({
    Forum: {
        screen: ForumScreen,
    },
    ForumThread: {
        screen: ForumThreadStack,
    },
}, {
    headerMode: 'none',
});

const InfoStack = StackNavigator({
    Info: {
        screen: InfoScreen,
    },
    InfoDetail: {
        screen: InfoDetailScreen,
    },
}, {
    headerMode: 'none',
});

const PictureStack = StackNavigator({
    PictureGallery: {
        screen: PictureGalleryScreen,
    },
    PictureDetail: {
        screen: PictureDetailScreen,
    },
    PictureTaker: {
        screen: PictureTakerScreen,
    },
}, {
    headerMode: 'none',
});

const MangroveNoteStack = StackNavigator({
    MangroveNoteReport: {
        screen: MangroveNoteReportScreen,
    },
    MangroveNoteInput: {
        screen: MangroveNoteInputScreen,
    },
}, {
    headerMode: 'none',
});

const MangroveGrowthStack = StackNavigator({
    MangroveGrowthReport: {
        screen: MangroveGrowthReportScreen,
    },
    MangroveGrowthInput: {
        screen: MangroveGrowthInputScreen,
    },
}, {
    headerMode: 'none',
});

const ShrimpStack = StackNavigator({
    ShrimpReport: {
        screen: ShrimpReportScreen,
    },
    ShrimpInput: {
        screen: ShrimpInputScreen,
    },
}, {
    headerMode: 'none',
});

const WaterQualityStack = StackNavigator({
    WaterQualityReport: {
        screen: WaterQualityReportScreen,
    },
    WaterQualityInput: {
        screen: WaterQualityInputScreen,
    },
}, {
    headerMode: 'none',
});

const MangroveMenuStack = StackNavigator({
    MangroveMenu: {
        screen: MangroveMenuScreen,
    },
    UpdateProgress: {
        screen: UpdateProgressScreen,
    },
    MangroveGrowth: {
        screen: MangroveGrowthStack,
    },
    MangroveNote: {
        screen: MangroveNoteStack,
    },
    PictureGallery: {
        screen: PictureStack,
    },
}, {
    headerMode: 'none',
});

const ClusterMenuStack = StackNavigator({
    ClusterMenu: {
        screen: ClusterMenu,
    },
    UpdateProgress: {
        screen: UpdateProgressScreen,
    },
    WaterQualityReport: {
        screen: WaterQualityStack,
    },
    ShrimpReport: {
        screen: ShrimpStack,
    },
    PictureGallery: {
        screen: PictureStack,
    },
}, {
    headerMode: 'none',
});

const HomeStack = StackNavigator({
    Home: {
        screen: HomeScreen,
    },
    ClusterMenu: {
        screen: ClusterMenuStack,
    },
    MangroveMenu: {
        screen: MangroveMenuStack,
    },
}, {
    headerMode: 'none',
});

const MainDrawer = DrawerNavigator({
    Home: {
        screen: HomeStack,
    },
    Info: {
        screen: InfoStack,
    },
    Forum: {
        screen: ForumStack,
    },
    Istilah: {
        screen: GlossaryScreen,
    },
    ChangePassword: {
        screen: ChangePasswordScreen,
    },
}, {
    contentComponent: CustomDrawer,
    drawerWidth,
});

export default MainDrawer;
