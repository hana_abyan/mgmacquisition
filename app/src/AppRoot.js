import React, { Component } from 'react';

import LoginScreen from './screens/LoginScreen';
import Navigator from './core/Navigator';

import * as sessionSelectors from './services/session/selectors';

export default class AppRoot extends Component {
    state = {
        isLoggedIn: false,
    }

    render() {
        if (sessionSelectors.getToken() || this.state.isLoggedIn) {
            return <Navigator />;
        }

        return <LoginScreen onLogin={() => this.setState({ isLoggedIn: true })} />;
    }
}
