import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension'

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import { reducer as clustersReducer } from './data/clusters/reducer';
import { reducer as articlesReducer } from './data/articles/reducer';
import { reducer as glossariesReducer } from './data/glossaries/reducer';
import { reducer as forumsReducer } from './data/forums/reducer';
import { reducer as servicesReducer } from './services/reducer';

const middlewares = [thunk];

if (__DEV__) {
    middlewares.push(createLogger());
}

const rootReducer = combineReducers({
    clusters: clustersReducer,
    articles: articlesReducer,
    glossaries: glossariesReducer,
    forums: forumsReducer,
    services: servicesReducer,
});

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2,
    debug: true,
    blacklist: ['threads'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, undefined, composeWithDevTools(applyMiddleware(...middlewares)));
export const persistor = persistStore(store);
