import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { store, persistor } from './app/src/store';

import AppRoot from './app/src/AppRoot';
import SplashScreen from './app/src/screens/SplashScreen';

const App = () => (
    <Provider store={store}>
        <PersistGate loading={<SplashScreen />} persistor={persistor}>
            <AppRoot />
        </PersistGate>
    </Provider>
);

export default App;
